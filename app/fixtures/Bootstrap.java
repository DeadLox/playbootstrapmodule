package fixtures;

import models.Configuration;
import models.Portee;
import models.Utilisateur;
import play.Logger;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 * Created with IntelliJ IDEA.
 * User: DeadLox
 * Date: 16/10/13
 * Time: 20:17
 * To change this template use File | Settings | File Templates.
 */
public class Bootstrap extends Job {

    public void doJob() {
        if (Configuration.count() == 0) {
            play.Logger.warn("Load configuration");
            Fixtures.loadModels("init-conf.yml");
        }
        if (Utilisateur.count() == 0) {
            Logger.warn("Load fixtures");
            Fixtures.loadModels("init-data.yml");
        }
    }
}
