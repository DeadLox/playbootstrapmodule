package portlet;

import models.Article;
import play.Logger;

import java.util.List;
import java.util.Set;

/**
 * Portlet QueryForeach permettant de lister des Articles
 *
 * Created by cleborgne on 06/11/2014.
 */
public class PortletQueryForeach {
    /** Titre de la Portlet Query Foreach */
    public String title;
    /** Liste des tags */
    public List<String> tags;
    /** Position de démarrage */
    public int start;
    /** Nombre maximum de resultat */
    public int limit;
    /** Collection des Articles  */
    public List<Article> articles;
    /** Total */
    public long total;
    /** Nombre total de pages */
    public int nbPage;

    public PortletQueryForeach(String title, int limit, List<String> tags){
        this.title = title;
        this.start = 0;
        this.limit = limit;
        this.tags = tags;
        calcTotal();
        calcNbPage();
        doRequest();
    }

    public PortletQueryForeach(String title, int start, int limit, List<String> tags){
        this.title = title;
        this.start = start;
        this.limit = limit;
        this.tags = tags;
        calcTotal();
        calcNbPage();
        doRequest();
    }

    public PortletQueryForeach(String title, List<Article> collection, int start, int limit, List<String> tags){
        this.title = title;
        this.start = start;
        this.limit = limit;
        this.tags = tags;
        calcTotal();
        calcNbPage();
        this.articles = collection;
    }

    /**
     * Effectue la requête pour recupérer les types de contenus
     */
    private void doRequest(){
        this.articles = Article.find("SELECT a FROM Article AS a JOIN a.category AS c WHERE c.title IN (?1) ORDER BY a.dateUpdate DESC", this.tags).from(this.start).fetch(this.limit);
    }

    /**
     * Calcul le total des résultats
     */
    private void calcTotal(){
        this.total = Article.count("SELECT COUNT(a) FROM Article AS a JOIN a.category AS c WHERE c.title IN (?1)", this.tags);
    }

    /**
     * Calcul le nombre de pages total
     */
    private void calcNbPage(){
        this.nbPage = (int) Math.ceil((double)total/(double)limit);
    }
}
