package interfaces;

/**
 * Created by cleborgne on 06/06/2014.
 */
public interface BootstrapConstants {
    /**
     * Liste des types de tentatives pour les formulaires
     */
    public static String ATTEMPT_LOGIN              = "LOGIN";
    public static String ATTEMPT_PASSWORD_RECOVERY  = "PASSWORD_RECOVERY";
    public static String ATTEMPT_PASSWORD_RESET     = "PASSWORD_RESET";
    public static String ATTEMPT_PASSWORD_CHANGE    = "PASSWORD_CHANGE";
    public static String ATTEMPT_CREATE_ACCOUNT     = "CREATE_ACCOUNT";
    public static String ATTEMPT_CONTACT            = "CONTACT";
    /**
     * Liste des types pour les logs
     */
    public static String LOG_TYPE_EMAIL           = "EMAIL";
    /**
     * Liste des sous-types de logs
     */
    public static String LOG_SOUS_TYPE_EMAIL_CONTACT            = "EMAIL_CONTACT";
    public static String LOG_SOUS_TYPE_EMAIL_NOTIFICATION       = "EMAIL_NOTIFICATION";
    public static String LOG_SOUS_TYPE_EMAIL_CREATE_ACCOUNT     = "EMAIL_CREATE_ACCOUNT";
    public static String LOG_SOUS_TYPE_EMAIL_NEWSLETTER         = "EMAIL_NEWSLETTER";
    public static String LOG_SOUS_TYPE_EMAIL_DELETE_ACCOUNT     = "EMAIL_DELETE_ACCOUNT";
    public static String LOG_SOUS_TYPE_EMAIL_FORGOT_PASSWORD    = "EMAIL_FORGOT_PASSWORD";

    // Nombre maximum de tentatives
    public static int ATTEMP_MAX_TENTATIVE          = 5;
    // Temps maximum pour le nombre maximum de requête - 5min
    public static int ATTEMPT_MAX_TIME              = 300;

    /** Types de configuration */
    public static String CONF_TYPE_TEXT             = "text";
    public static String CONF_TYPE_BOOL             = "boolean";
    public static String CONF_TYPE_DATE             = "date";
    public static String CONF_TYPE_TIME             = "time";
    public static String CONF_TYPE_DATETIME         = "datetime";
    public static String CONF_TYPE_INT              = "int";
    public static String CONF_TYPE_ARRAY            = "array";

    /** Boolean */
    public static String BOOL_TRUE                  = "true";
    public static String BOOL_FALSE                 = "false";
}
