package notifiers;

import interfaces.BootstrapConstants;
import models.Utilisateur;
import play.i18n.Messages;
import play.mvc.Mailer;
import util.ConfigurationUtil;
import util.LogsUtil;

import java.util.Arrays;
import java.util.List;

/**
 * Created by DeadLox on 14/07/2014.
 */
public class BootstrapMailer extends Mailer {

    /**
     * Envoie un email de réinitialisation du mot de passe
     * @param user
     */
    public static void forgotPassword(Utilisateur user){
        setFrom(ConfigurationUtil.getStringValue("email.form.from"));
        setSubject(Messages.get("secure.forgot.mail.subject"));
        addRecipient(user.email);
        LogsUtil.addLog(BootstrapConstants.LOG_TYPE_EMAIL, BootstrapConstants.LOG_SOUS_TYPE_EMAIL_FORGOT_PASSWORD, user.email, Messages.get("secure.forgot.mail.subject"), "Forgot pasword");
        send(user);
    }

    /**
     * Création d'un nouveau compte
     * @param user
     */
    public static void createAccount(Utilisateur user){
        setFrom(ConfigurationUtil.getStringValue("email.form.from"));
        setSubject(Messages.get("secure.create.account.subject"));
        addRecipient(user.email);
        LogsUtil.addLog(BootstrapConstants.LOG_TYPE_EMAIL, BootstrapConstants.LOG_SOUS_TYPE_EMAIL_CREATE_ACCOUNT, user.email, Messages.get("secure.create.account.subject"), "Create account");
        send(user);
    }

    /**
     * Création d'un nouveau compte, envoie d'une alerte à l'administrateur
     * @param user
     */
    public static void createAccountAlert(Utilisateur user){
        setFrom(ConfigurationUtil.getStringValue("email.form.from"));
        setSubject(Messages.get("secure.create.account.alert.subject"));
        addRecipient(ConfigurationUtil.getStringValue("email.form.to"));
        LogsUtil.addLog(BootstrapConstants.LOG_TYPE_EMAIL, BootstrapConstants.LOG_SOUS_TYPE_EMAIL_CREATE_ACCOUNT, ConfigurationUtil.getStringValue("email.form.to"), Messages.get("secure.create.account.alert.subject"), "Create account");
        send(user);
    }

    /**
     * Création d'un nouveau compte, envoie d'une alerte à l'administrateur
     * @param user
     */
    public static void deleteAccountAlert(Utilisateur user){
        setFrom(ConfigurationUtil.getStringValue("email.form.from"));
        setSubject(Messages.get("secure.delete.account.alert.subject"));
        addRecipient(ConfigurationUtil.getStringValue("email.form.to"));
        LogsUtil.addLog(BootstrapConstants.LOG_TYPE_EMAIL, BootstrapConstants.LOG_SOUS_TYPE_EMAIL_DELETE_ACCOUNT, ConfigurationUtil.getStringValue("email.form.to"), Messages.get("secure.delete.account.alert.subject"), "Delete account");
        send(user);
    }

    /**
     * Demande de contact
     * @param email
     * @param sujet
     * @param message
     */
    public static void contact(String email, String sujet, String message){
        setFrom(email);
        setSubject(Messages.get("contact.email.sujet", sujet));
        addRecipient(ConfigurationUtil.getStringValue("email.form.to"));
        LogsUtil.addLog(BootstrapConstants.LOG_TYPE_EMAIL, BootstrapConstants.LOG_SOUS_TYPE_EMAIL_CONTACT, email, sujet, message);
        send(message);
    }

    /**
     * Envoi d'une Newsletter
     * @param utilisateurs La liste des destinataires
     * @param sujet Le sujet
     * @param message Le message
     */
    public static void newsletter(List<Utilisateur> utilisateurs, String sujet, String message){
        setFrom(ConfigurationUtil.getStringValue("email.form.from"));
        setSubject(sujet);
        addRecipient(Messages.get("email.undisclosed.recipients", ConfigurationUtil.getStringValue("email.form.to")));
        String destinataires ="";
        for (Utilisateur utilisateur : utilisateurs) {
            addBcc(utilisateur.email);
            if (destinataires==null) {
                destinataires = utilisateur.email;
            } else {
                destinataires += ";"+utilisateur.email;
            }
        }
        LogsUtil.addLog(BootstrapConstants.LOG_TYPE_EMAIL, BootstrapConstants.LOG_SOUS_TYPE_EMAIL_NEWSLETTER, destinataires, sujet, message);
        send(sujet, message);
    }

    /**
     * Notification
     * @param user L'utilisateur traité
     * @param sujet Le sujet de la notification
     * @param message Le message de la notification
     */
    public static void notification(Utilisateur user, String sujet, String message){
        setFrom(ConfigurationUtil.getStringValue("email.form.to"));
        setSubject(Messages.get("notification.email.sujet", sujet));
        addRecipient(user.email);
        LogsUtil.addLog(BootstrapConstants.LOG_TYPE_EMAIL, BootstrapConstants.LOG_SOUS_TYPE_EMAIL_NOTIFICATION, user.email, Messages.get("notification.email.sujet", sujet), message);
        send(user, message);
    }
}
