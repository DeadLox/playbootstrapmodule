package notifiers;

import play.i18n.Messages;
import play.mvc.Mailer;
import util.ConfigurationUtil;

/**
 * Created by DeadLox on 24/05/2014.
 */
public class TestMail extends Mailer {

    /**
     * Envoi un email de test
     * @param to
     * @param subject
     * @param message
     */
    public static void sendBasicTest(String to, String subject, String message){
        setFrom(ConfigurationUtil.getStringValue("email.form.from"));
        setSubject(subject);
        addRecipient(to);
        send(subject, message);
    }
}
