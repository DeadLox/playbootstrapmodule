package controllers;

import models.Category;
import models.CloudinaryImage;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import play.db.Model;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.Router;
import play.mvc.With;
import util.CloudinaryUtil;
import util.ModuleUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cleborgne on 26/05/2014.
 */
@With({Secure.class, Online.class})
@Check("Contributeur")
public class CloudinaryImages extends CRUD {
    private static Logger logger = Logger.getLogger(CloudinaryImages.class);

    public static void create(String libelle, File image){
        if (!ModuleUtil.cloudinaryEnable()) {
            flash.error(Messages.get("cloudinary.not.configured"));
        } else {
            if (request.method.equals("POST")) {
                long categoryId = 0;
                String categoryIdStr = params.get("category.id");
                if (categoryIdStr != null) {
                    categoryId = Long.parseLong(categoryIdStr);
                }
                validation.required(image);
                if (validation.hasErrors()) {
                    flash.error("Merci de remplir tous les champs");
                } else {
                    // Upload vers Cloudinary
                    CloudinaryImage cloudinaryImage = CloudinaryUtil.uploadImage(libelle, image);
                    if (cloudinaryImage != null) {
                        if (categoryId != 0) {
                            Category category = Category.findById(categoryId);
                            if (category != null) {
                                cloudinaryImage.category = category;
                                cloudinaryImage.save();
                            }
                        }
                        flash.success(Messages.get("cloudinary.upload.success"));
                        redirect("CloudinaryImages.list");
                    } else {
                        flash.error(Messages.get("cloudinary.upload.error"));
                    }
                }
            }
        }
        render();
    }

    /**
     * Crée plusieurs images Cloudinary
     * @param images
     */
    public static void createMultiple(File[] images){
        if (!ModuleUtil.cloudinaryEnable()) {
            flash.error(Messages.get("cloudinary.not.configured"));
        } else {
            if (request.method.equals("POST")) {
                long categoryId = 0;
                String categoryIdStr = params.get("category.id");
                if (categoryIdStr != null) {
                    categoryId = Long.parseLong(categoryIdStr);
                }
                validation.required(images);
                if (validation.hasErrors()) {
                    flash.error("Merci de remplir tous les champs");
                } else {
                    boolean hasErrors = false;
                    for (File image : images) {
                        // Upload vers Cloudinary
                        CloudinaryImage cloudinaryImage = CloudinaryUtil.uploadImage(image);
                        if (cloudinaryImage != null) {
                            if (categoryId != 0) {
                                Category category = Category.findById(categoryId);
                                if (category != null) {
                                    cloudinaryImage.category = category;
                                    cloudinaryImage.save();
                                }
                            }
                        } else {
                            hasErrors = true;
                        }
                    }
                    if (hasErrors) {
                        flash.error(Messages.get("cloudinary.upload.multiple.error"));
                    } else {
                        flash.success(Messages.get("cloudinary.upload.multiple.success"));
                        redirect("CloudinaryImages.list");
                    }
                }
            }
        }
        render();
    }

    /**
     * Modifie une CLoudinaryImage
     * @param libelle
     * @param image
     */
    public static void save(long id, String libelle, File image){
        if (!ModuleUtil.cloudinaryEnable()) {
            flash.error(Messages.get("cloudinary.not.configured"));
        } else {
            if (request.method.equals("POST")) {
                long categoryId = 0;
                String categoryIdStr = params.get("category.id");
                if (categoryIdStr != null) {
                    categoryId = Long.parseLong(categoryIdStr);
                }
                if (image == null) {
                    // Si seuelement le titre ou la catégorie ont été changés
                    CloudinaryImage cloudinaryImage = CloudinaryImage.findById(id);
                    if (cloudinaryImage != null) {
                        cloudinaryImage.libelle = libelle;
                        Category category = Category.findById(categoryId);
                        if (category != null) {
                            cloudinaryImage.category = category;
                        }
                        cloudinaryImage.save();
                        flash.success(Messages.get("cloudinary.edit.success"));
                    }
                } else {
                    // Si l'image a été changée
                    // Upload vers Cloudinary
                    CloudinaryImage cloudinaryImage = CloudinaryUtil.uploadImage(libelle, image);
                    if (cloudinaryImage != null) {
                        if (categoryId != 0) {
                            Category category = Category.findById(categoryId);
                            if (category != null) {
                                cloudinaryImage.category = category;
                                cloudinaryImage.save();
                            }
                        }
                        flash.success(Messages.get("cloudinary.upload.success"));
                    } else {
                        flash.error(Messages.get("cloudinary.upload.error"));
                    }
                }
            }
        }

        redirect("CloudinaryImages.list");
    }

    /**
     * Supprime une image à partir de son ID
     * @param id
     * @throws Exception
     */
    public static void delete(long id) throws Exception {
        JSONObject result = new JSONObject();
        CloudinaryImage cloudinaryImage = CloudinaryImage.findById(id);
        if (cloudinaryImage != null) {
            String resultDelete = CloudinaryUtil.delete(cloudinaryImage);
            if (resultDelete.equals(CloudinaryUtil.SUCCESS)) {
                flash.success(Messages.get("cloudinary.delete.success"));
            } else {
                flash.error(Messages.get("cloudinary.delete.error", resultDelete));
            }
        } else {
            flash.error(Messages.get("cloudinary.delete.image.unknow"));
        }

        redirect("CloudinaryImages.list");
    }
}
