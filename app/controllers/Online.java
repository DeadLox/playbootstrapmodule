package controllers;

import models.Utilisateur;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import util.BootstrapUtil;
import util.ConfigurationUtil;

/**
 * Created by DeadLox on 11/01/15.
 */
public class Online extends Controller {

    @Before
    public static void update(){
        if (ConfigurationUtil.getBoolValue("logs.online.enable")) {
            Utilisateur user = Secure.Security.getLoggedMember();
            models.Online online = new models.Online(user);
            String username = (user != null) ? user.toString() : Messages.get("logs.online.user.guest");
            if (models.Online.count("ip = ? OR username = ?", BootstrapUtil.getRealIpFromHeroku(), username) > 0) {
                models.Online.delete("ip = ? OR username = ?", BootstrapUtil.getRealIpFromHeroku(), username);
            }
            online.save();
            models.Online.cleanOlder();
        }
    }
}
