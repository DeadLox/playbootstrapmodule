package controllers;

import interfaces.BootstrapConstants;
import models.Etat;
import models.Role;
import models.Utilisateur;
import notifiers.BootstrapMailer;
import org.apache.log4j.Logger;
import play.data.validation.Error;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.With;
import play.utils.Java;
import util.BCrypt;
import util.BootstrapUtil;
import util.CaptchaUtil;
import util.ConfigurationUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by DeadLox on 20/07/2014.
 */
@With(Online.class)
public class SecurityAccount extends Controller {
    private static Logger logger = Logger.getLogger(SecurityAccount.class);

    /**
     * Vérifie si la réinitialisation du mot de passe est activé
     */
    private static void checkCreateAccountEnabled(){
        if (!ConfigurationUtil.getBoolValue("application.create.account")) {
            flash.error(Messages.get("application.create.account.disable"));
            Application.index();
        }
    }

    /**
     * Effectue la vérification du captcha si celui-ci est activé
     */
    private static void checkCaptcha(){
        if (CaptchaUtil.captchaEnable()) {
            if (!CaptchaUtil.checkCaptcha()) {
                params.flash();
                validation.keep();
                create();
            }
        }
    }

    /**
     * Affichage du formulaire de création de compte
     */
    public static void create(){
        checkCreateAccountEnabled();
        render();
    }

    /**
     * Création d'un nouveau compte
     * @param pseudo
     * @param email
     * @param password
     * @param passwordConfirm
     */
    public static void saveCreate(String pseudo, String email, String password, String passwordConfirm){
        checkCreateAccountEnabled();
        if (BootstrapUtil.checkAttempt(BootstrapConstants.ATTEMPT_CREATE_ACCOUNT)) {
            checkCaptcha();
            validation.required(pseudo);
            validation.required(email);
            validation.email(email);
            validation.required(password);
            validation.required(passwordConfirm);
            validation.equals(password, passwordConfirm);
            if (validation.hasErrors()) {
                params.flash();
                validation.keep();
                create();
            }

            Utilisateur user = new Utilisateur();
            user.pseudo = pseudo;
            user.email = email;
            user.password = BCrypt.hashpw(password, BCrypt.gensalt());
            user.roles.add(Role.getRoleMembre());
            user.etat = Etat.getwaiting();
            user.dateCreation = new Date();

            validation.valid(user);
            if (validation.hasErrors()) {
                for (Map.Entry<String, List<Error>> entry : validation.errorsMap().entrySet()) {
                    if (entry.getKey().contains("pseudo")) {
                        for (Error error : entry.getValue()) {
                            if (error.message().equals(Messages.get("validation.unique"))) {
                                validation.addError("pseudo", "secure.create.account.unique.pseudo");
                            } else {
                                validation.addError("pseudo", error.message());
                            }
                        }
                    }
                    if (entry.getKey().contains("email")) {
                        for (Error error : entry.getValue()) {
                            if (error.message().equals(Messages.get("validation.unique"))) {
                                validation.addError("email", "secure.create.account.unique.email");
                            } else {
                                validation.addError("email", error.message());
                            }
                        }
                    }
                    if (entry.getKey().contains("password")) {
                        for (Error error : entry.getValue()) {
                            validation.addError("password", error.message());
                        }
                    }
                }
                params.flash();
                validation.keep();
                create();
            }

            // Génération du code d'activation
            BootstrapUtil.generateUniqueCode(user);

            try {
                SecurityAccountOver.invoke("persist", user);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            BootstrapMailer.createAccount(user);
            // Envoie d'une alerte à l'administrateur
            if (ConfigurationUtil.getBoolValue("email.admin.alert")) {
                BootstrapMailer.createAccountAlert(user);
            }

            flash.success(Messages.get("secure.create.account.success"));
        } else {
            flash.error(Messages.get("secure.attempt.max", BootstrapUtil.formatSeconds(BootstrapConstants.ATTEMPT_MAX_TIME)));
            params.flash();
            create();
        }
        Application.index();
    }

    /**
     * Activation du compte
     * @param email
     * @param code
     */
    public static void activate(String email, String code){
        checkCreateAccountEnabled();
        Utilisateur user = Utilisateur.find("email = ? AND code = ?", email, code).first();
        if (user == null) {
            flash.error(Messages.get("secure.activate.account.error"));
            Application.index();
        }

        if (user.etat.isEnable()) {
            flash.error(Messages.get("secure.account.already.active"));
            Application.index();
        }

        if (user.etat.isDisable()) {
            flash.error(Messages.get("secure.account.disable"));
            Application.index();
        }

        if (user.etat.isDelete()) {
            flash.error(Messages.get("secure.account.delete"));
            Application.index();
        }

        // Vérifie la validité du code
        BootstrapUtil.checkCodeValidity(user.codeDate);

        user.code = "";
        user.codeDate = null;
        user.etat = Etat.getEnable();
        user.dateActivation = new Date();
        user.save();

        flash.success(Messages.get("secure.activate.success"));

        Application.index();
    }

    public static class SecurityAccountOver extends Controller {

        static void persist(Utilisateur user){
            user.save();
        }

        public static Object invoke(String m, Object... args) throws Throwable {
            try {
                return Java.invokeChildOrStatic(SecurityAccountOver.class, m, args);
            } catch(InvocationTargetException e) {
                throw e.getTargetException();
            }
        }
    }
}
