package controllers;

import interfaces.BootstrapConstants;
import models.Utilisateur;
import notifiers.BootstrapMailer;
import org.apache.log4j.Logger;
import org.joda.time.Interval;
import play.i18n.Messages;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.With;
import util.BCrypt;
import util.BootstrapUtil;
import util.CaptchaUtil;
import util.ConfigurationUtil;

import java.util.Date;

/**
 * Created by DeadLox on 19/07/2014.
 */
@With(Online.class)
public class SecurityPwd extends Controller {
    private static Logger logger = Logger.getLogger(SecurityPwd.class);

    /**
     * Vérifie si la réinitialisation du mot de passe est activé
     */
    private static void checkForgotEnabled(){
        if (!ConfigurationUtil.getBoolValue("application.forgot")) {
            flash.error(Messages.get("application.forgot.disable"));
            Application.index();
        }
    }

    /**
     * Effectue la vérification du captcha si celui-ci est activé
     */
    private static void checkCaptcha(){
        if (CaptchaUtil.captchaEnable()) {
            if (!CaptchaUtil.checkCaptcha()) {
                params.flash();
                validation.keep();
                forgot("");
            }
        }
    }

    /**
     * Envoie d'un email de reinitialisation de mot de passe
     */
    public static void forgot(String email){
        checkForgotEnabled();
        if (request.method.equals("POST")) {
            if (BootstrapUtil.checkAttempt(BootstrapConstants.ATTEMPT_PASSWORD_RECOVERY)) {
                checkCaptcha();
                validation.required(email);
                validation.email(email);
                if (validation.hasErrors()) {
                    flash.error(Messages.get("secure.forgot.email.not.valid"));
                } else {
                    Utilisateur user = Utilisateur.find("byEmail", email).first();
                    if (user == null) {
                        render();
                    } else {
                        BootstrapUtil.generateUniqueCode(user);
                        user.save();
                        BootstrapMailer.forgotPassword(user);
                        flash.success(Messages.get("secure.forgot.email.send"));
                    }
                }
            } else {
                flash.error(Messages.get("secure.attempt.max", BootstrapUtil.formatSeconds(BootstrapConstants.ATTEMPT_MAX_TIME)));
                params.flash();
            }
        }
        render();
    }

    /**
     * Formulaire permettant la réinitialisation du mot de passe
     * @param email
     * @param code
     */
    public static void reset(String email, String code){
        checkForgotEnabled();
        if (BootstrapUtil.checkAttempt(BootstrapConstants.ATTEMPT_PASSWORD_RESET)) {
            validation.required(email);
            validation.email(email);
            validation.required(code);
            if (!validation.hasErrors()) {
                Utilisateur user = Utilisateur.find("email = ? AND code = ?", email, code).first();
                if (user != null) {
                    // Vérifie la validité du code
                    BootstrapUtil.checkCodeValidity(user.codeDate);
                    render(user);
                }
            }
            flash.error(Messages.get("secure.reset.error"));
        } else {
            flash.error(Messages.get("secure.attempt.max", BootstrapUtil.formatSeconds(BootstrapConstants.ATTEMPT_MAX_TIME)));
            params.flash();
        }
        Application.index();
    }

    /**
     * Post du formulaire
     */
    public static void change(String email, String code, String newPassword, String newPasswordConfirm){
        checkForgotEnabled();
        if (BootstrapUtil.checkAttempt(BootstrapConstants.ATTEMPT_PASSWORD_CHANGE)) {
            validation.required(email);
            validation.email(email);
            validation.required(code);
            if (!validation.hasErrors()) {
                Utilisateur user = Utilisateur.find("email = ? AND code = ?", email, code).first();
                if (user != null) {
                    // Vérifie la validité du code
                    BootstrapUtil.checkCodeValidity(user.codeDate);

                    // Vérifie le nouveau mot de passe
                    validation.required(newPassword);
                    validation.required(newPasswordConfirm);
                    validation.equals(newPassword, newPasswordConfirm);
                    if (validation.hasErrors()) {
                        renderTemplate("SecurityPwd/reset.html", user);
                        reset(email, code);
                    }

                    // On hash le nouveau mot de passe avec BCrypt
                    user.password = BCrypt.hashpw(newPassword, BCrypt.gensalt());
                    user.codeDate = null;
                    user.code = "";
                    user.save();

                    flash.success(Messages.get("secure.reset.success"));

                    Application.index();
                }
            }
            flash.error(Messages.get("secure.reset.error"));
        } else {
            flash.error(Messages.get("secure.attempt.max", BootstrapUtil.formatSeconds(BootstrapConstants.ATTEMPT_MAX_TIME)));
            params.flash();
            reset(email, code);
        }
        Application.index();
    }
}
