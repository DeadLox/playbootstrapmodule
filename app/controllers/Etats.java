package controllers;

import play.mvc.With;

/**
 * Created by cleborgne on 22/05/2014.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class Etats extends CRUD {
}
