package controllers;

import models.Role;
import models.Utilisateur;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import util.ConfigurationUtil;

/**
 * Controlleur permettant de basculer le site en mode maintenance
 * Le site reste accessible pour les utilisateurs authenetifiés ayant le niveau requis
 *
 * Created by DeadLox on 25/11/14.
 */
public class Maintenance extends Controller {

    @Before(unless = {"show"})
    public static void check(){
        if (checkMode() && !canAccess()) {
            redirect("Maintenance.show");
        }
    }

    /**
     * Affiche la page de maintenance si le mode est activé
     */
    public static void show(){
        if (checkMode() && !canAccess()) {
            render();
        } else {
            redirect("Application.index");
        }
    }


    /**
     * Vérifie si le mode maintenance est activé
     * @return <code>TRUE</code> si le mode maintenance est activé
     */
    private static boolean checkMode(){
        // Si le mode Maintenance est activé
        if (ConfigurationUtil.getBoolValue("maintenance.enable")) {
            return true;
        }
        return false;
    }

    /**
     * Vérifie si l'utilisateur connecté peut accèder au site en maintenance
     * Retourne TRUE si:
     * - L'utilisateur possède le rôle suffissant
     * - L'utilisateur est Adminsitrateur
     *
     * Si le rôle minimum n'est pas renseigné, seul les Adminsitrateurs ont accès à la totalité du site en maintenance
     *
     * @return <code>TRUE</code> si l'utilisateur connecté peut accéder au site en maintenance
     */
    private static boolean canAccess(){
        Utilisateur loggedMember = Secure.Security.getLoggedMember();
        if (loggedMember != null) {
            if (!loggedMember.isAdmin()) {
                String roleNameMin = ConfigurationUtil.getStringValue("maintenance.role.access");
                if (roleNameMin != null && !roleNameMin.equals("")) {
                    Role roleMin = Role.find("byNom", roleNameMin).first();
                    if (roleMin != null) {
                        if (!loggedMember.roles.contains(roleMin)) {
                            return true;
                        }
                    }
                }
            } else {
                return true;
            }
        }
        return false;
    }
}
