package controllers;

import interfaces.BootstrapConstants;
import models.Attempt;
import models.Log;
import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.With;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by cleborgne on 06/06/2014.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class AdminApp extends BootstrapController {

    public static void statut(){
        Set<String> propertiesSet = Play.configuration.stringPropertyNames();
        List<String> properties = new ArrayList<String>(propertiesSet);
        Collections.sort(properties);
        render(properties);
    }

    /**
     * Efface la totalité des logs
     * @param typeLog
     */
    public static void cleanLog(String typeLog){
        Log.delete("type = ?", typeLog);
        connexion();
    }

    /**
     * Affiche les logs des tentatives de connexion
     */
    public static void connexion(){
        List<Attempt> attemptList = Attempt.find("ORDER BY date DESC").fetch();
        render(attemptList);
    }

    /**
     * Affiche les logs des envois d'email
     */
    public static void email(){
        List<Log> logList = Log.find("ORDER BY dateUpdate DESC").fetch();
        render(logList);
    }

    /**
     * Affiche les logs des utilisateurs en ligne
     */
    public static void online(){
        List<models.Online> onlineList = models.Online.findAll();
        render(onlineList);
    }
}
