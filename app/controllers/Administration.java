package controllers;

import models.Data;
import org.json.JSONObject;
import play.Logger;
import play.db.jpa.Model;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.With;
import util.AlgoliaUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by cleborgne on 28/04/14.
 */
@With({Secure.class, Online.class})
@Check("Contributeur")
public class Administration extends BootstrapController {

    public static void index(){
        render();
    }
}
