package controllers;

import models.Category;
import models.CloudinaryFile;
import models.CloudinaryImage;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.With;
import util.CloudinaryUtil;
import util.ModuleUtil;

import java.io.File;

/**
 * Created by cleborgne on 26/05/2014.
 */
@With({Secure.class, Online.class})
@Check("Contributeur")
public class CloudinaryFiles extends CRUD {
    private static Logger logger = Logger.getLogger(CloudinaryFiles.class);

    public static void create(String libelle, File file){
        if (!ModuleUtil.cloudinaryEnable()) {
            flash.error(Messages.get("cloudinary.not.configured"));
        } else {
            if (request.method.equals("POST")) {
                long categoryId = 0;
                String categoryIdStr = params.get("category.id");
                if (categoryIdStr != null) {
                    categoryId = Long.parseLong(categoryIdStr);
                }
                validation.required(file);
                if (validation.hasErrors()) {
                    flash.error("Merci de remplir tous les champs");
                } else {
                    // Upload vers Cloudinary
                    CloudinaryFile cloudinaryFile = CloudinaryUtil.uploadRaw(libelle, file);
                    if (cloudinaryFile != null) {
                        if (categoryId != 0) {
                            Category category = Category.findById(categoryId);
                            if (category != null) {
                                cloudinaryFile.category = category;
                                cloudinaryFile.save();
                            }
                        }
                        flash.success(Messages.get("cloudinary.upload.success"));
                        redirect("CloudinaryFiles.list");
                    } else {
                        flash.error(Messages.get("cloudinary.upload.error"));
                    }
                }
            }
        }
        render();
    }

    /**
     * Crée plusieurs fichiers Cloudinary
     * @param files
     */
    public static void createMultiple(File[] files){
        if (!ModuleUtil.cloudinaryEnable()) {
            flash.error(Messages.get("cloudinary.not.configured"));
        } else {
            if (request.method.equals("POST")) {
                long categoryId = 0;
                String categoryIdStr = params.get("category.id");
                if (categoryIdStr != null) {
                    categoryId = Long.parseLong(categoryIdStr);
                }
                validation.required(files);
                if (validation.hasErrors()) {
                    flash.error("Merci de remplir tous les champs");
                } else {
                    boolean hasErrors = false;
                    for (File file : files) {
                        // Upload vers Cloudinary
                        CloudinaryFile cloudinaryFile = CloudinaryUtil.uploadRaw(file.getName(), file);
                        if (cloudinaryFile != null) {
                            if (categoryId != 0) {
                                Category category = Category.findById(categoryId);
                                if (category != null) {
                                    cloudinaryFile.category = category;
                                    cloudinaryFile.save();
                                }
                            }
                        } else {
                            hasErrors = true;
                        }
                    }
                    if (hasErrors) {
                        flash.error(Messages.get("cloudinary.upload.multiple.error"));
                    } else {
                        flash.success(Messages.get("cloudinary.upload.multiple.success"));
                        redirect("CloudinaryFiles.list");
                    }
                }
            }
        }
        render();
    }

    /**
     * Modifie une CLoudinaryFile
     * @param libelle
     * @param file
     */
    public static void save(long id, String libelle, File file){
        if (!ModuleUtil.cloudinaryEnable()) {
            flash.error(Messages.get("cloudinary.not.configured"));
        } else {
            if (request.method.equals("POST")) {
                long categoryId = 0;
                String categoryIdStr = params.get("category.id");
                if (categoryIdStr != null) {
                    categoryId = Long.parseLong(categoryIdStr);
                }
                if (file == null) {
                    // Si seuelement le titre ou la catégorie ont été changés
                    CloudinaryFile cloudinaryFile = CloudinaryFile.findById(id);
                    if (cloudinaryFile != null) {
                        cloudinaryFile.libelle = libelle;
                        Category category = Category.findById(categoryId);
                        if (category != null) {
                            cloudinaryFile.category = category;
                        }
                        cloudinaryFile.save();
                        flash.success(Messages.get("cloudinary.edit.success"));
                    }
                } else {
                    // Si le fichier a été changée
                    // Upload vers Cloudinary
                    CloudinaryFile cloudinaryFile = CloudinaryUtil.uploadRaw(libelle, file);
                    if (cloudinaryFile != null) {
                        if (categoryId != 0) {
                            Category category = Category.findById(categoryId);
                            if (category != null) {
                                cloudinaryFile.category = category;
                                cloudinaryFile.save();
                            }
                        }
                        flash.success(Messages.get("cloudinary.upload.success"));
                    } else {
                        flash.error(Messages.get("cloudinary.upload.error"));
                    }
                }
            }
        }

        redirect("CloudinaryFiles.list");
    }

    /**
     * Supprime une image à partir de son ID
     * @param id
     * @throws Exception
     */
    public static void delete(long id) throws Exception {
        JSONObject result = new JSONObject();
        CloudinaryFile cloudinaryFile = CloudinaryFile.findById(id);
        if (cloudinaryFile != null) {
            String resultDelete = CloudinaryUtil.delete(cloudinaryFile);
            if (resultDelete.equals(CloudinaryUtil.SUCCESS)) {
                flash.success(Messages.get("cloudinary.delete.success"));
            } else {
                flash.error(Messages.get("cloudinary.delete.error", resultDelete));
            }
        } else {
            flash.error(Messages.get("cloudinary.delete.image.unknow"));
        }

        redirect("CloudinaryFiles.list");
    }
}
