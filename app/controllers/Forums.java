package controllers;

import models.Category;
import models.Forum;
import play.cache.Cache;
import play.data.binding.Binder;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;
import play.mvc.With;

import java.lang.reflect.Constructor;

/**
 * Created by cleborgne on 26/01/2015.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class Forums extends CRUD {

    public static void blank(long id) throws Exception {
        Category category = Category.findById(id);
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Forum object = (Forum) constructor.newInstance();
        object.category = category;
        try {
            render(type, object);
        } catch (TemplateNotFoundException e) {
            render("CRUD/blank.html", type, object);
        }
    }

    public static void list() {
        Category  rootCat = Category.find("byTitle", "Forum").first();
        render(rootCat);
    }

    public static void create() throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Forum object = (Forum) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.auteur = Security.getLoggedMember();
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();
        // Suppression du cache des compteurs
        Cache.delete(type.name + ".count");
        flash.success(Messages.get("crud.created", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        if (params.get("_saveAndAddAnother") != null) {
            redirect(request.controller + ".blank");
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void save(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Forum object = (Forum) type.findById(id);
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.auteur = Security.getLoggedMember();
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        // Suppression du cache des compteurs
        Cache.delete(type.name + ".count");
        flash.success(Messages.get("crud.saved", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        redirect(request.controller + ".show", object._key());
    }
}
