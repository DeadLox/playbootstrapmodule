package controllers;

import models.*;
import play.data.binding.Binder;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import util.BoardUtil;
import util.Pagination;

/**
 * Created by cleborgne on 26/01/2015.
 */
public class Board extends Controller {


    /** *************************** Droits d'accès ************************* **/

    /**
     * Vérifie les droits d'accès au Forum
     */
    @Before
    public static void checkAccess(){
        if (!BoardUtil.canAccessForum()) {
            flash.error(Messages.get("secure.access.denied"));
            Application.index();
        }
    }

    /**
     * Vérifie que l'utilisateur peut voir le Forum
     */
    @Before(only = {"forum", "lockForum"})
    public static void checkForumAccess(){
        if (params._contains("id")) {
            Forum forum = Forum.findById(Long.parseLong(params.get("id")));
            if (forum != null) {
                Role viewRole = forum.viewableBy;
                Utilisateur user = Security.getLoggedMember();
                Role roleUser = user.getHighterRole();
                if (viewRole != null && viewRole.level > roleUser.level) {
                    flash.error(Messages.get("forum.access.denied", forum));
                    Board.index();
                }
            } else {
                flash.error(Messages.get("forum.not.exist", params.get("id")));
                Board.index();
            }
        } else {
            flash.error(Messages.get("forum.not.exist", ""));
            Board.index();
        }
    }

    /**
     * Vérifie que l'utilisateur peut créer un Topic dans le Forum
     */
    @Before(only = {"createTopic"})
    public static void checkTopicCreate(){
        if (params._contains("id")) {
            Forum forum = Forum.findById(Long.parseLong(params.get("id")));
            if (forum != null) {
                if (!forum.locked) {
                    Role contribRole = forum.contribBy;
                    Utilisateur user = Security.getLoggedMember();
                    Role roleUser = user.getHighterRole();
                    if (contribRole != null && contribRole.level > roleUser.level) {
                        flash.error(Messages.get("forum.create.denied", forum));
                        Board.forum(forum.id);
                    }
                } else {
                    flash.error(Messages.get("forum.locked", forum));
                    Board.forum(forum.id);
                }
            } else {
                flash.error(Messages.get("forum.not.exist", params.get("id")));
                Board.index();
            }
        } else {
            flash.error(Messages.get("forum.not.exist", ""));
            Board.index();
        }
    }

    /**
     * Vérifie que l'utilisateur peut voir le Topic
     */
    @Before(only = {"topic", "editTopic", "lockTopic"})
    public static void checkTopicAccess(){
        if (params._contains("id")) {
            Topic topic = Topic.findById(Long.parseLong(params.get("id")));
            if (topic != null) {
                Role viewRole = topic.viewableBy;
                Utilisateur user = Security.getLoggedMember();
                Role roleUser = user.getHighterRole();
                if (topic == null || viewRole == null || viewRole.level > roleUser.level) {
                    flash.error(Messages.get("forum.topic.access.denied", topic.titre));
                    Board.forum(topic.forum.id);
                }
            } else {
                flash.error(Messages.get("forum.topic.not.exist", params.get("id")));
                Board.index();
            }
        }  else {
            flash.error(Messages.get("forum.topic.not.exist", ""));
            Board.index();
        }
    }

    /**
     * Vérifie que l'utilisateur peut créer un post dans ce Topic
     */
    @Before(only = "createPost")
    public static void checkPostCreate(){
        if (params._contains("id")) {
            Topic topic = Topic.findById(Long.parseLong(params.get("id")));
            if (topic != null) {
                if (!topic.forum.locked) {
                    if (!topic.locked) {
                        Role contribRole = topic.contribBy;
                        Utilisateur user = Security.getLoggedMember();
                        Role roleUser = user.getHighterRole();
                        if (topic == null || contribRole == null || contribRole.level > roleUser.level) {
                            flash.error(Messages.get("forum.topic.create.denied", topic.titre));
                            Board.topic(topic.id);
                        }
                    } else {
                        flash.error(Messages.get("forum.topic.locked", topic));
                        Board.topic(topic.id);
                    }
                } else {
                    flash.error(Messages.get("forum.locked", topic.forum));
                    Board.topic(topic.id);
                }
            } else {
                flash.error(Messages.get("forum.topic.not.exist", params.get("id")));
                Board.index();
            }
        }  else {
            flash.error(Messages.get("forum.topic.not.exist", ""));
            Board.index();
        }
    }

    /** *************************** FO ************************* **/

    /**
     * Page d'accueil du Forum
     */
    public static void index(){
        Category rootCat = Category.find("byTitle", "Forum").first();
        render(rootCat);
    }

    /**
     * Affiche un forum
     * @param id L'id du Forum
     */
    public static void forum(long id){
        Forum forum = Forum.findById(id);
        Utilisateur loggedMember = Security.getLoggedMember();
        Role highterUserRole = loggedMember.getHighterRole();
        Pagination pagination = new Pagination(request);
        pagination.setTotal(Topic.count("SELECT count(t) FROM Topic t WHERE forum = ? AND (viewableBy IS NULL OR viewableBy.level <= ?)", forum, highterUserRole.level));
        pagination.setCollection(Topic.find("SELECT t FROM Topic t WHERE forum = ? AND (viewableBy IS NULL OR viewableBy.level <= ?) ORDER BY dateUpdate DESC", forum, highterUserRole.level).from(pagination.getStart()).fetch(pagination.getLimit()));
        render(forum, pagination);
    }

    /**
     * Affiche un topic
     * @param id L'id du Topic
     */
    public static void topic(long id){
        Topic topic = Topic.findById(id);

        // Nombre de vues
        topic.views++;
        topic.save();

        // On met à jour les derniers Topics et post vus par l'utilisateur connecté
        ForumView.updateUserView(topic);

        Pagination pagination = new Pagination(request);
        pagination.setTotal(topic.posts.size());
        // On affiche la dernière page
        pagination.setCurrentPage(pagination.getNbPage());
        pagination.setCollection(Post.find("topic = ? ORDER BY dateCreate ASC", topic).from(pagination.getStart()).fetch(pagination.getLimit()));
        render(topic, pagination);
    }

    /** *************************** FO Formulaires ************************* **/

    /**
     * Ajout d'un nouveau Topic en FO
     * @param id L'id du Forum dans lequel sera créé le Topic
     */
    public static void createTopic(long id){
        Forum forum = Forum.findById(id);
        if (forum != null) {
            if (request.method.equalsIgnoreCase("POST")) {
                // Topic
                Topic topic = new Topic();
                topic.auteur = Security.getLoggedMember();
                topic.forum = forum;
                // Par défaut, on affecte les droits du Forum parent
                topic.viewableBy = forum.viewableBy;
                topic.contribBy  = forum.contribBy;
                topic.editableBy = forum.editableBy;
                Binder.bindBean(params.getRootParamNode(), "topic", topic);
                validation.valid(topic);
                if (validation.hasErrors()) {
                    renderArgs.put("error", Messages.get("crud.hasErrors"));
                    render(forum);
                }

                // Post
                Post post = new Post();
                post.auteur = Security.getLoggedMember();
                post.topic = topic;
                Binder.bindBean(params.getRootParamNode(), "post", post);
                validation.valid(post);
                if (validation.hasErrors()) {
                    renderArgs.put("error", Messages.get("crud.hasErrors"));
                    render(forum);
                }

                // Save
                topic.save();
                post.save();
                topic.posts.add(post);
                topic.save();

                flash.success(Messages.get("crud.created", topic));
                topic(topic.id);
            }
        }
        render(forum);
    }

    /**
     * Edition d'un Topic en FO
     * @param id L'id du Topic édité
     */
    public static void editTopic(long id){
        Topic topic = Topic.findById(id);
        if (request.method.equalsIgnoreCase("POST")) {
            Binder.bindBean(params.getRootParamNode(), "topic", topic);
            validation.valid(topic);
            if (validation.hasErrors()) {
                renderArgs.put("error", Messages.get("crud.hasErrors"));
                render(topic);
            }

            // post
            Post post = topic.getFirstPost();
            Binder.bindBean(params.getRootParamNode(), "post", post);
            validation.valid(post);
            if (validation.hasErrors()) {
                renderArgs.put("error", Messages.get("crud.hasErrors"));
                render(topic);
            }

            // Save
            topic.save();
            post.save();
            topic.posts.add(post);
            topic.save();

            flash.success(Messages.get("crud.saved", topic));
            topic(topic.id);
        }
        render(topic);
    }

    /**
     * Ajout d'un nouveau Post en FO
     * @param id L'id du Topic dans lequel sera créé le Post
     */
    public static void createPost(long id){
        Topic topic = Topic.findById(id);
        if (topic != null) {
            if (request.method.equalsIgnoreCase("POST")) {
                // Post
                Post post = new Post();
                post.auteur = Security.getLoggedMember();
                post.topic = topic;
                Binder.bindBean(params.getRootParamNode(), "post", post);
                validation.valid(post);
                if (validation.hasErrors()) {
                    renderArgs.put("error", Messages.get("crud.hasErrors"));
                    render(topic);
                }

                // Save
                post.save();
                topic.posts.add(post);
                topic.save();

                // On met à jour les derniers Topics et post vus par l'utilisateur connecté
                ForumView.updateUserPostInTopic(topic);

                // Mise à jour des stats du Forum
                Forum forum = topic.forum;
                forum.updateStat(post);
                forum.save();

                flash.success(Messages.get("crud.created", topic));
                topic(topic.id);
            }
        }
        render(topic);
    }

    /** *************************** Manage ************************* **/

    /**
     * Inverse le vérrouillage d'un Forum
     * @param id L'id du Forum
     */
    public static void lockForum(long id){
        Forum forum = Forum.findById(id);
        if (forum != null) {
            forum.locked = !forum.locked;
            forum.save();

            flash.success(Messages.get("forum.lock.success", forum, "forum.lock." + forum.locked));
            forum(forum.id);
        } else {
            flash.error(Messages.get("forum.not.exist", id));
            index();
        }
    }

    /**
     * Inverse le vérrouillage d'un Topic
     * @param id L'id du Topic
     */
    public static void lockTopic(long id){
        Topic topic = Topic.findById(id);
        if (topic != null) {
            topic.locked = !topic.locked;
            topic.save();

            flash.success(Messages.get("forum.topic.lock.success", topic, "forum.lock."+topic.locked));
            topic(topic.id);
        } else {
            flash.error(Messages.get("forum.topic.not.exist", id));
            index();
        }
    }
}
