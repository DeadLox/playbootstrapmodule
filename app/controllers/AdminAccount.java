package controllers;

import models.Utilisateur;
import org.apache.log4j.Logger;
import play.data.validation.Required;
import play.i18n.Messages;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.With;
import util.BCrypt;
import util.BootstrapUtil;
import util.ConfigurationUtil;
import util.ImageUtil;

import java.io.File;

/**
 * Created by cleborgne on 06/06/2014.
 */
@With({Secure.class, Online.class})
@Check("Contributeur")
public class AdminAccount extends BootstrapController {
    private static Logger logger = Logger.getLogger(AdminAccount.class);

    /**
     * Affiche le prodil de l'utilisateur
     */
    public static void profil(){
        Utilisateur user = Secure.Security.getLoggedMember();
        render(user);
    }

    /**
     * Formulaire de modification des informations du profil
     */
    public static void info(){
        Utilisateur user = Secure.Security.getLoggedMember();
        render(user);
    }

    public static void saveInfo(String pseudo, String email){
        Utilisateur user = Secure.Security.getLoggedMember();

        CRUD.ObjectType type = new CRUD.ObjectType(Utilisateur.class);
        notFoundIfNull(type);
        Utilisateur object = Secure.Security.getLoggedMember();
        notFoundIfNull(object);

        validation.required(pseudo);
        validation.required(email);
        validation.email(email);
        if (validation.hasErrors()) {
            validation.keep();
            flash.error(Messages.get("check.all.field"));
            info();
        }

        user.pseudo = pseudo;
        user.email = email;
        user.save();
        flash.success(Messages.get("account.info.save"));

        info();
    }

    /**
     * Formulaire de modification du mot de passe
     */
    public static void password(){
        Utilisateur user = Secure.Security.getLoggedMember();
        render(user);
    }

    public static void savePassword(String oldPassword, String newPassword, String newPasswordConfirm){
        Utilisateur user = Secure.Security.getLoggedMember();

        CRUD.ObjectType type = new CRUD.ObjectType(Utilisateur.class);
        notFoundIfNull(type);
        Utilisateur object = Secure.Security.getLoggedMember();
        notFoundIfNull(object);

        boolean isMD5 = BootstrapUtil.isValidMD5(object.password);

        if (isMD5) {
            String oldPasswordMD5 = Codec.hexMD5(oldPassword);

            // Vérifie l'ancien mot de passe
            validation.required(oldPassword);
            validation.equals(oldPasswordMD5, object.password);
            if (validation.hasErrors()) {
                params.flash();
                flash.put("oldPasswordError", Messages.get("utilisateur.password.not.equal"));
                render("AdminAccount/password.html", type, object);
            }

            // Vérifie le nouveau mot de passe
            validation.required(newPassword);
            validation.required(newPasswordConfirm);
            validation.equals(newPassword, newPasswordConfirm);
            if (validation.hasErrors()) {
                params.flash();
                renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
                render("AdminAccount/password.html", type, object);
            }

            // On hash le nouveau mot de passe avec BCrypt
            object.password = BCrypt.hashpw(newPassword, BCrypt.gensalt());
        } else {
            // BCrypt
            // Vérifie l'ancien mot de passe
            validation.required(oldPassword);
            validation.isTrue(BCrypt.checkpw(oldPassword, object.password));
            if (validation.hasErrors()) {
                params.flash();
                flash.put("oldPasswordError", Messages.get("utilisateur.password.not.equal"));
                render("AdminAccount/password.html", type, object);
            }

            // Vérifie le nouveau mot de passe
            validation.required(newPassword);
            validation.required(newPasswordConfirm);
            validation.equals(newPassword, newPasswordConfirm);
            if (validation.hasErrors()) {
                params.flash();
                renderArgs.put("error", play.i18n.Messages.get("crud.hasErrors"));
                render("AdminAccount/password.html", type, object);
            }

            // Vérifie que le nouveau mot de passe n'est pas égale à l'ancien
            if (BCrypt.checkpw(newPassword, object.password)) {
                params.flash();
                flash.put("newPasswordError", Messages.get("utilisateur.password.equal.last"));
                render("AdminAccount/password.html", type, object);
            }

            // On hash le nouveau mot de passe avec BCrypt
            object.password = BCrypt.hashpw(newPassword, BCrypt.gensalt());
        }
        renderTemplate("AdminAccount/password.html", user);
    }

    /**
     * Gestion de l'avatar de l'utilisateur
     * @param avatar
     */
    public static void avatar(File avatar){
        Utilisateur user = Secure.Security.getLoggedMember();
        String formats = ConfigurationUtil.getStringValue("avatar.ext");
        int maxWidth = ConfigurationUtil.getIntValue("avatar.width.limit");
        int maxHeight = ConfigurationUtil.getIntValue("avatar.height.limit");
        ImageUtil.uploadAvatar(avatar);
        render(user, formats, maxWidth, maxHeight);
    }
}
