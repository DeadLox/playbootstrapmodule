package controllers;

import models.Etat;
import models.Utilisateur;
import notifiers.BootstrapMailer;
import play.Logger;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;
import play.mvc.With;
import util.BCrypt;
import util.BootstrapUtil;
import util.ConfigurationUtil;

import java.lang.reflect.Constructor;

/**
 * Created by cleborgne on 28/04/14.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class Utilisateurs extends CRUD {

    /**
     * Supprime l'avatar de l'utilisateur
     * @param id L'id de l'utilisateur
     * @throws Exception
     */
    public static void deleteAvatar(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Utilisateur object = (Utilisateur) type.findById(id);
        notFoundIfNull(object);
        try {
            object.removeAvatar();
        } catch (Exception e) {
            flash.error(Messages.get("crud.delete.error", "Avatar"));
        }
        flash.success(Messages.get("crud.deleted", "Avatar"));
        redirect("/admin/account/avatar");
    }

    /**
     * Lors de la modification d'un utilisateur par le CRUD
     *
     * @param id L'id de l'utilisateur modifié
     * @throws Exception
     */
    public static void save(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Utilisateur object = (Utilisateur) type.findById(id);
        notFoundIfNull(object);

        // Récupération des précédentes valeur
        Etat lastEtat = object.etat;

        Binder.bindBean(params.getRootParamNode(), "object", object);
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }

        // Si l'état de l'utilisateur a changé, envoi d'une notification par email
        Etat newEtat = object.etat;
        if (!lastEtat.equals(newEtat)) {
            String message = Messages.get("notification.etat.change.message", newEtat.css, newEtat.libelle);
            BootstrapMailer.notification(object, Messages.get("notification.etat.change.sujet"), message);
        }

        try {
            SecurityAccountOver.invoke("persist", object);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        flash.success(Messages.get("utilisateur.saved", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        redirect(request.controller + ".show", object._key());
    }

    /**
     * Lors de la création d'un nouvel utilisateur par le crud
     *
     * @throws Exception
     */
    public static void create() throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Utilisateur object = (Utilisateur) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }

        object.password = BCrypt.hashpw(object.password, BCrypt.gensalt());

        try {
            SecurityAccountOver.invoke("persist", object);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        flash.success(Messages.get("utilisateur.created", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        if (params.get("_saveAndAddAnother") != null) {
            redirect(request.controller + ".blank");
        }
        redirect(request.controller + ".show", object._key());
    }

    /**
     * Suppression d'un compte
     * @param id L'id de l'utilisateur
     * @throws Exception
     */
    public static void delete(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Utilisateur object = (Utilisateur) type.findById(id);
        notFoundIfNull(object);

        // On affecte le rôle supprimé
        object.etat = Etat.getDelete();
        object.save();

        // Envoie d'une alerte à l'administrateur
        if (ConfigurationUtil.getBoolValue("email.admin.alert")) {
            BootstrapMailer.deleteAccountAlert(object);
        }

        flash.success(Messages.get("user.etat.disable", object));
        redirect(request.controller + ".list");
    }
}
