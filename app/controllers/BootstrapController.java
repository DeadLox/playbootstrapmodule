package controllers;

import play.mvc.Controller;
import play.mvc.With;

/**
 * Created by cleborgne on 09/02/2015.
 */
@With(Compress.class)
public class BootstrapController extends Controller {
}
