package controllers;

import models.Article;
import models.Category;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;
import play.mvc.With;

import java.lang.reflect.Constructor;

/**
 * Created by DeadLox on 28/10/14.
 */
@With({Secure.class, Online.class})
public class Articles extends CRUD {

    /**
     * Affichage un article en FullDisplay
     * @param id
     */
    @Check("Guest")
    public static void full(long id){
        Article article = Article.findById(id);
        notFoundIfNull(article);
        render(article);
    }

    /**
     * Création d'un nouvel Article
     * @param categoryId
     * @throws Exception
     */
    @Check("Contributeur")
    public static void create(long categoryId) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Article object = (Article) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        // Persiste la catégorie
        if (categoryId != 0) {
            Category category = Category.findById(categoryId);
            if (category != null){
                object.category = category;
            }
        }
        object.auteur = Security.getLoggedMember();
        object._save();
        flash.success(Messages.get("crud.created", type.modelName));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        if (params.get("_saveAndAddAnother") != null) {
            redirect(request.controller + ".blank");
        }
        redirect(request.controller + ".show", object._key());
    }

    /**
     * Update d'un Article
     * @param id Son Id
     * @param categoryId L'id de la catégorie
     * @throws Exception
     */
    @Check("Contributeur")
    public static void save(String id, long categoryId) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Article object = (Article) type.findById(id);
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        // Persiste la catégorie
        if (categoryId != 0) {
            Category category = Category.findById(categoryId);
            if (category != null){
                object.category = category;
            }
        }
        object.auteur = Security.getLoggedMember();
        object._save();
        flash.success(Messages.get("crud.saved", type.modelName));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        redirect(request.controller + ".show", object._key());
    }
}
