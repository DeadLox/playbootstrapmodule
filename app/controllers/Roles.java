package controllers;

import play.mvc.With;

/**
 * Created by cleborgne on 28/04/14.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class Roles extends CRUD {
}
