package controllers;

import models.Forum;
import models.Post;
import models.Topic;
import play.cache.Cache;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;

import java.lang.reflect.Constructor;
import java.util.Date;

/**
 * Created by cleborgne on 20/02/2015.
 */
public class Posts extends CRUD {

    public static void create() throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Post object = (Post) constructor.newInstance();
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.auteur = Security.getLoggedMember();
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();

        // Met à jour le Topic
        Topic topic = object.topic;
        topic.dateUpdate = new Date();
        topic.save();
        // Met à jour le Forum
        Forum forum = object.topic.forum;
        forum.nbPosts++;
        forum.lastPost = object;
        forum.save();


        // Suppression du cache des compteurs
        Cache.delete(type.name+".count");
        flash.success(Messages.get("crud.created", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        if (params.get("_saveAndAddAnother") != null) {
            redirect(request.controller + ".blank");
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void save(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Post object = (Post) type.findById(id);
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.auteur = Security.getLoggedMember();
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();

        // Met à jour le Topic
        Topic topic = object.topic;
        topic.dateUpdate = new Date();
        topic.save();
        // Met à jour le Forum
        Forum forum = object.topic.forum;
        forum.nbPosts++;
        forum.lastPost = object;
        forum.save();

        // Suppression du cache des compteurs
        Cache.delete(type.name + ".count");
        flash.success(Messages.get("crud.saved", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        redirect(request.controller + ".show", object._key());
    }
}
