package controllers;

import play.Logger;
import play.mvc.Controller;

import java.util.List;

/**
 * Created by cleborgne on 02/06/2014.
 */
public class Navigation extends Controller {

    /**
     * Retourne <code>TRUE</code> si la requête contient l'un des mots
     * @param pathList La liste de mot à tester
     * @return <code>TRUE</code>
     */
    public static boolean checkPath(List<String> pathList){
        String requestPath = request.path;
        for (String path : pathList) {
            if (requestPath.contains(path)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retourne <code>TRUE</code> si le chemin de la requête commence par la chaine passée en paramètre
     * @param startPath La chaine
     * @return <code>TRUE</code> si le chemin commence par la chaine
     */
    public static boolean startWith(String startPath){
        String requestPath = request.path;
        return requestPath.startsWith(startPath);
    }
}
