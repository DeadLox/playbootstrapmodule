package controllers;

import models.Post;
import models.Topic;
import play.cache.Cache;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;

import java.lang.reflect.Constructor;

/**
 * Created by cleborgne on 20/02/2015.
 */
public class Topics extends CRUD {

    public static void create() throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Topic object = (Topic) constructor.newInstance();

        notFoundIfNull(object);

        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.auteur = Security.getLoggedMember();
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        object._save();

        // On sauvegarde le Topic après son insertion en base
        // Création du post associé au Topic
        String postContent = params.get("post");
        Post post = new Post();
        post.auteur = object.auteur;
        post.contenu = postContent;
        post.topic = object;
        validation.required(postContent);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/blank.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/blank.html", type, object);
            }
        }
        post.save();

        // Met à jour les stats du forum
        object.forum.updateStat(post);

        // Suppression du cache des compteurs
        Cache.delete(type.name + ".count");
        flash.success(Messages.get("crud.created", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        if (params.get("_saveAndAddAnother") != null) {
            redirect(request.controller + ".blank");
        }
        redirect(request.controller + ".show", object._key());
    }

    public static void save(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Topic object = (Topic) type.findById(id);
        notFoundIfNull(object);
        Binder.bindBean(params.getRootParamNode(), "object", object);
        object.auteur = Security.getLoggedMember();
        validation.valid(object);
        if (validation.hasErrors()) {
            renderArgs.put("error", Messages.get("crud.hasErrors"));
            try {
                render(request.controller.replace(".", "/") + "/show.html", type, object);
            } catch (TemplateNotFoundException e) {
                render("CRUD/show.html", type, object);
            }
        }
        object._save();
        // Suppression du cache des compteurs
        Cache.delete(type.name+".count");
        flash.success(Messages.get("crud.saved", object));
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        redirect(request.controller + ".show", object._key());
    }
}
