package controllers;

import models.Etat;
import models.Role;
import models.Utilisateur;
import notifiers.BootstrapMailer;
import notifiers.TestMail;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.With;
import util.ModuleUtil;
import util.NewsletterUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cleborgne on 18/08/2014.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class Newsletter extends BootstrapController {
    private static Logger logger = Logger.getLogger(Newsletter.class);

    public static void form(String[] to, String sujet, String message) {
        if (!ModuleUtil.mailEnable()) {
            flash.error(Messages.get("email.not.configured"));
        }
        if (request.method.equals("POST")) {
            validation.required(to);
            validation.required(sujet);
            validation.required(message);
            if (validation.hasErrors()) {
                flash.error("Foiré");
                params.flash();
                render();
            }

            // Récupère la liste des destinataires par rapport aux tags
            List<Utilisateur> destinataires = NewsletterUtil.getListTo(to);

            BootstrapMailer.newsletter(destinataires, sujet, message);

            flash.success(Messages.get("email.test.success"));
        }
        render();
    }

    /**
     * Retourne la liste des tags au format JSON
     */
    public static void getToValues(){
        JSONArray values = new JSONArray();

        try {
            // Tous les utilisateurs
            JSONObject allUsers = new JSONObject();
            allUsers.put("value", NewsletterUtil.OPTION_ALL_USERS);
            allUsers.put("text", Messages.get("newsletter.all.users"));
            allUsers.put("type", NewsletterUtil.TYPE_OPTION);
            values.put(allUsers);
            // Tous les rôles
            JSONObject allRoles = new JSONObject();
            allRoles.put("value", NewsletterUtil.OPTION_ALL_ROLES);
            allRoles.put("text", Messages.get("newsletter.all.roles"));
            allRoles.put("type", NewsletterUtil.TYPE_OPTION);
            values.put(allRoles);
            // Tous les états
            JSONObject allEtats = new JSONObject();
            allEtats.put("value", NewsletterUtil.OPTION_ALL_ETATS);
            allEtats.put("text", Messages.get("newsletter.all.etats"));
            allEtats.put("type", NewsletterUtil.TYPE_OPTION);
            values.put(allEtats);

            // Ajoute les utilisateurs
            List<Utilisateur> users = Utilisateur.findAll();
            for (Utilisateur user : users) {
                JSONObject userJSON = new JSONObject();
                userJSON.put("value", user.id);
                userJSON.put("text", user);
                userJSON.put("type", NewsletterUtil.TYPE_USER);
                values.put(userJSON);
            }

            // Ajoute les rôles
            List<Role> roles = Role.findAll();
            for (Role role : roles) {
                JSONObject roleJSON = new JSONObject();
                roleJSON.put("value", role.id);
                roleJSON.put("text", role);
                roleJSON.put("type", NewsletterUtil.TYPE_ROLE);
                values.put(roleJSON);
            }

            // Ajoute les états
            List<Etat> etats = Etat.findAll();
            for (Etat etat : etats) {
                JSONObject roleJSON = new JSONObject();
                roleJSON.put("value", etat.id);
                roleJSON.put("text", etat);
                roleJSON.put("type", NewsletterUtil.TYPE_ETAT);
                values.put(roleJSON);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        renderJSON(values.toString());
    }
}
