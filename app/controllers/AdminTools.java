package controllers;

import com.cloudinary.Cloudinary;
import com.google.gson.JsonElement;
import models.CloudinaryImage;
import notifiers.TestMail;
import org.apache.commons.io.IOUtils;
import org.jboss.netty.handler.codec.http.HttpMethod;
import play.Logger;
import play.i18n.Messages;
import play.libs.Codec;
import play.libs.WS;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.With;
import util.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by cleborgne on 04/06/2014.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class AdminTools extends BootstrapController {

    /******************************************************/
    /**                    Kimonolabs                    **/
    /******************************************************/

    /**
     * Permet de récupérer des informations de l'API Kimonolabs
     */
    public static void kimonolabs(){
        render();
    }

    /******************************************************/
    /**             Génération mot de passe              **/
    /******************************************************/

    /**
     * Retourne le mot de passe crypté
     * @param password
     */
    public static void genPassword(String password){
        if (password != null) {
            if (params.get("_random") != null) {
                String codec = Codec.UUID();
                password = codec.substring(0, codec.indexOf("-")).toLowerCase().trim();
            }
            String passwordEncoded = BCrypt.hashpw(password, BCrypt.gensalt());
            render(password, passwordEncoded);
        }
        render();
    }

    /******************************************************/
    /**                     Embed.ly                     **/
    /******************************************************/

    /**
     * Affiche l'url embed.ly d'une url
     * Permet de contourner les proxy
     */
    public static void embed(String urlOrigin){
        if (urlOrigin != null) {
            String urlEmbed = Embed.getEmbedLink(urlOrigin);
            render(urlOrigin, urlEmbed);
        }
        render(urlOrigin);
    }


    /******************************************************/
    /**             Test d'envoi de mail                 **/
    /******************************************************/

    /**
     * Envoie un email de test
     * @param to
     * @param subject
     * @param message
     */
    public static void emailTest(String to, String subject, String message){
        if (!ModuleUtil.mailEnable()) {
            flash.error(Messages.get("email.not.configured"));
        }
        if (request.method.equals("POST")) {
            validation.required(to);
            validation.required(subject);
            validation.required(message);
            if (validation.hasErrors()) {
                flash.error("Foiré");
                params.flash();
                render();
            }

            TestMail.sendBasicTest(to, subject, message);
            flash.success(Messages.get("email.test.success"));
        }
        render();
    }

    /******************************************************/
    /**                 Test Cloudinary                  **/
    /******************************************************/
    /**
     * Récupère les informations du compte Cloudinary si il est activé
     */
    public static void cloudinary(){
        if (!ModuleUtil.cloudinaryEnable()) {
            flash.error(Messages.get("cloudinary.not.configured"));
        }
        Cloudinary cloudinary = CloudinaryUtil.getCloudinary();
        try {
            Map usage = cloudinary.api().usage(Cloudinary.emptyMap());
            Logger.info("Usage", usage);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.warn("Impossible de contacter le serveur Cloudinary pour obtenir l'usage du compte", "");
        }
        render();
    }

    /******************************************************/
    /**                 Google Analytics                 **/
    /******************************************************/
    public static void googleAnalytics(){
        if (!ModuleUtil.gaEnable()) {
            flash.error(Messages.get("ga.not.configured"));
        }
        render();
    }

    /******************************************************/
    /**                 Algolia Search                   **/
    /******************************************************/
    public static void algoliaAPI(){
        if (!ModuleUtil.algoliaSearchEnable()) {
            flash.error(Messages.get("algolia.not.configured"));
        }
        if (request.method.equals("POST")) {
            if (AlgoliaUtil.test()) {
                flash.success(Messages.get("algolia.test.success"));
            } else {
                flash.error(Messages.get("algolia.test.error"));
            }
        }
        render();
    }

    public static void algoliaIndex(){
        if (!ModuleUtil.algoliaSearchEnable()) {
            flash.error(Messages.get("algolia.not.configured"));
            algoliaAPI();
        }
        List<Map<String, String>> indexes = AlgoliaUtil.getIndexInfo();
        List<Class> indexClass = IndexUtil.getAllJsonClass();
        render(indexes, indexClass);
    }

    public static void algoliaSearch(String recherche, String index){
        if (!ModuleUtil.algoliaSearchEnable()) {
            flash.error(Messages.get("algolia.not.configured"));
            algoliaAPI();
        }
        List<Map<String, String>> indexes = AlgoliaUtil.getIndexInfo();
        if (request.method.equals("POST")) {
            validation.required(recherche);
            validation.required(index);
            if (validation.hasErrors()) {
                flash.error(Messages.get("algolia.search.empty"));
                render(indexes);
            }
            List<Map<String,String>> listHits = AlgoliaUtil.doSearch(recherche, index);
            render(indexes, listHits, recherche);
        }
        render(indexes);
    }

    /**
     * Réindexe tous les types de contenus
     */
    public static void indexAllType(){
        if (AlgoliaUtil.indexAllType()){
            flash.success(Messages.get("algolia.index.all.success"));
        } else {
            flash.error(Messages.get("algolia.index.all.error"));
        }
        AdminTools.algoliaIndex();
    }

    /**
     * Permet la réindexation d'une classe
     * @param clazz Le nom de la classe à réindexer
     */
    public static void indexType(String clazz){
        if (AlgoliaUtil.indexType(clazz)) {
            flash.success(Messages.get("algolia.index.success", clazz));
        } else {
            flash.error(Messages.get("algolia.index.error", clazz));
        }
        AdminTools.algoliaIndex();
    }
}
