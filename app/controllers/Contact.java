package controllers;

import interfaces.BootstrapConstants;
import notifiers.BootstrapMailer;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import util.BootstrapUtil;
import util.CaptchaUtil;
import util.ConfigurationUtil;

/**
 * Created by DeadLox on 20/07/2014.
 */
@With(Online.class)
public class Contact extends BootstrapController {

    /**
     * Affichage du formaulaire de contact
     */
    public static void form(){
        render();
    }

    /**
     * Vérifie si le formulaire de contact est activé
     */
    @Before(unless = "checkCaptcha")
    public static void checkContactEnable(){
        if (!ConfigurationUtil.getBoolValue("application.contact.enable")) {
            flash.error(Messages.get("application.fonctionnalite.disable"));
            Application.index();
        }
    }

    /**
     * Effectue la vérification du captcha si celui-ci est activé
     */
    private static void checkCaptcha(){
        if (CaptchaUtil.captchaEnable()) {
            if (!CaptchaUtil.checkCaptcha()) {
                params.flash();
                validation.keep();
                form();
            }
        }
    }

    /**
     * Envoie du formulaire de contact
     */
    public static void send(String email, String sujet, String message){
        checkContactEnable();
        if (BootstrapUtil.checkAttempt(BootstrapConstants.ATTEMPT_CONTACT)) {
            checkCaptcha();
            validation.required(email);
            validation.email(email);
            validation.required(sujet);
            validation.required(message);
            if (validation.hasErrors()) {
                params.flash();
                validation.keep();
                form();
            }

            BootstrapMailer.contact(email, sujet, message);

            flash.success(Messages.get("contact.success"));
        }  else {
            flash.error(Messages.get("secure.attempt.max", BootstrapUtil.formatSeconds(BootstrapConstants.ATTEMPT_MAX_TIME)));
            params.flash();
        }

        form();
    }
}
