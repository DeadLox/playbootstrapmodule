package controllers;

import play.mvc.Controller;
import play.mvc.With;

/**
 * Created by cleborgne on 06/08/2014.
 */
@With(Online.class)
public class Version extends BootstrapController {

    public static void history(){
        render();
    }
}
