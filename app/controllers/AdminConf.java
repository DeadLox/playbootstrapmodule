package controllers;

import interfaces.BootstrapConstants;
import models.Configuration;
import models.Portee;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.With;
import play.test.Fixtures;
import util.BootstrapFixtures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by cleborgne on 06/06/2014.
 */
@With({Secure.class, Online.class})
@Check("Administrateur")
public class AdminConf extends BootstrapController implements BootstrapConstants {

    public static void index(){
        List<Portee> portees = Portee.findAll();
        Collections.sort(portees);
        List<Configuration> confs = new ArrayList<Configuration>();
        // Filtre par type
        if (request.method.equals("POST")) {
            String portee = params.get("portee");
            if (portee != null && !portee.equals("")) {
                try {
                    Portee porteeSelected = Portee.find("byLibelle", portee).first();
                    confs = Configuration.find("byPortee", porteeSelected).fetch();
                    Collections.sort(confs);
                    render(portees, porteeSelected, confs);
                } catch (IllegalArgumentException e) {
                    flash.error(Messages.get("conf.illegal.type"));
                }
            }
        }
        confs = Configuration.findAll();
        Collections.sort(confs);
        render(portees, confs);
    }

    /**
     * Recharge entiérement les configurations depuis le fichier par défaut (init-conf.yml)
     */
    public static void reload(){
        Configuration.deleteAll();
        Portee.deleteAll();
        Fixtures.delete(Configuration.class);
        Fixtures.delete(Portee.class);
        play.Logger.warn("Reset configuration");
        Fixtures.loadModels("init-conf.yml");
        flash.success(Messages.get("conf.reload.success"));
        index();
    }

    /**
     * Met à jour les configurations depuis le fichier par défaut (init-conf.yml)
     * Seules les nouvelles configurations sont ajoutées
     * Les configurations existantes ne sont pas écrasées/modifiées
     */
    public static void update(){
        List<Configuration> configurations = Configuration.findAll();
        play.Logger.warn("Update configuration");
        BootstrapFixtures.updateModels("init-conf.yml");
        flash.success(Messages.get("conf.update.success"));
        index();
    }

    public static void edit(long id){
        Configuration conf = Configuration.findById(id);
        if (request.method.equals("POST")) {
            String value = params.get("conf.value");
            if (conf.type.equals(CONF_TYPE_BOOL)) {
                if (value != null) {
                    conf.value = BOOL_TRUE;
                } else {
                    conf.value = BOOL_FALSE;
                }
            } else {
                conf.value = value;
            }
            conf.save();
            flash.success(Messages.get("conf.save.success"));
            index();
        }
        render(conf);
    }
}
