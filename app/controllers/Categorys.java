package controllers;

import models.Category;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import play.cache.Cache;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.i18n.Messages;
import play.mvc.With;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Set;

/**
 * Created by DeadLox on 02/08/2014.
 */
@With({Secure.class, Online.class})
public class Categorys extends CRUD {
    private static Logger logger = Logger.getLogger(Categorys.class);

    /**
     * Affiche la liste des catégories à partir de la racine
     */
    @Check("Contributeur")
    public static void list(){
        List<Category> listCategory = Category.find("parent IS NULL").fetch();
        Category rootCategory = null;
        if (listCategory.size() > 0) {
            rootCategory = listCategory.iterator().next();
        }
        render(rootCategory);
    }

    /**
     * Convertit une catégorie en JSON
     * @param category
     * @return
     */
    @Check("Contributeur")
    private static JSONObject convertToJson(Category category, List<Long> check){
        JSONObject catJson = new JSONObject();
        try {
            // Title
            catJson.put("text", category.title);

            // ID
            catJson.put("id", category.id);

            // State
            JSONObject catStateJson = new JSONObject();
            if (check.contains(category.id)) {
                catStateJson.put("selected", true);
            }
            catJson.put("state", catStateJson);
            catJson.put("type", "category");

            // Children
            if (category.childrenSet.size() > 0) {
                JSONArray catChildJson = new JSONArray();
                for (Category subCategory : category.childrenSet) {
                    catChildJson.put(convertToJson(subCategory, check));
                }
                catJson.put("children", catChildJson);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return catJson;
    }

    /**
     * Convertit une liste de categories en JSON
     * @param childrenSet
     * @return StringBuffer sb
     */
    @Check("Contributeur")
    private static JSONArray convertChildrenToJson(Set<Category> childrenSet, List<Long> check){
        JSONArray jsonArray = new JSONArray();
            for (Category category : childrenSet) {
                jsonArray.put(convertToJson(category, check));
            }
        return jsonArray;
    }

    /**
     * Retourne l'arborescence à partir de la catégorie passée en paramètre
     */
    @Check("Contributeur")
    public static void load(long id, List<Long> check){
        JSONArray jsonArray = new JSONArray();
        if (id == 0) {
            List<Category> listCategory = Category.find("parent IS NULL").fetch();
            for (Category cat : listCategory) {
                jsonArray.put(convertToJson(cat, check));
            }
        } else {
            Category category = Category.findById(id);
            jsonArray = convertChildrenToJson(category.childrenSet, check);
        }
        renderText(jsonArray);
    }

    /**
     * Ajoute une catégorie racine ou une sous-catégorie
     * @param id L'id de la catégorie parent
     *           0 pour la racine
     * @throws Exception
     */
    @Check("Contributeur")
    public static void ajouter(Long id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Constructor<?> constructor = type.entityClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Category object = (Category) constructor.newInstance();
        Category parent = Category.findById(id);
        object.parent = parent;
        render(type, object);
    }

    /**
     * Déplace une catégorie
     * @param id L'id de la catégorie déplacée
     * @param parentId L'id de la catégorie parente
     */
    @Check("Contributeur")
    public static void update(long id, long parentId){
        Category category = Category.findById(id);
        if (category != null) {
            if (parentId != 0) {
                // Sous-catégorie
                Category parentCat = Category.findById(parentId);
                category.parent = parentCat;
                category.save();
            } else {
                // Racine
                category.parent = null;
                category.save();
            }
        }
    }

    /**
     * Supprime une catégorie
     * @param id
     * @throws Exception
     */
    @Check("Contributeur")
    public static void supprimer(String id) throws Exception {
        ObjectType type = ObjectType.get(getControllerClass());
        notFoundIfNull(type);
        Category object = (Category) type.findById(id);
        notFoundIfNull(object);

        object.deleteAllContent();
        object.save();

        try {
            object.delete();
        } catch (Exception e) {
            flash.error(play.i18n.Messages.get("crud.delete.error", object));
            redirect(request.controller + ".list");
        }
        flash.success(play.i18n.Messages.get("crud.deleted", object));
        redirect(request.controller + ".list");
    }
}
