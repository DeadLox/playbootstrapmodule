package util;

import controllers.Secure;
import models.CloudinaryImage;
import models.Utilisateur;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.libs.Images;
import play.mvc.Http;
import play.mvc.Scope;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by DeadLox on 22/06/2014.
 */
public class ImageUtil {

    /**
     * Upload une photo pour l'utilisateur connecté
     * @param avatar Le fichier à uploader
     */
    public static void uploadAvatar(File avatar){
        Http.Request request = Http.Request.current();
        Validation validation = Validation.current();
        Scope.Flash flash = Scope.Flash.current();
        Utilisateur user = Secure.Security.getLoggedMember();
        if (request.method.equals("POST")) {
            validation.required(avatar);
            if (validation.hasErrors()) {
                flash.error(Messages.get("account.avatar.empty"));
            } else {
                List<String> exts = ConfigurationUtil.getStringArrayValue("avatar.ext");
                if (BootstrapUtil.checkExtensionFile(avatar, exts)) {
                    avatar = ImageUtil.resizeAvatar(avatar);
                    CloudinaryImage avatarCloudinary = CloudinaryUtil.uploadImage(Messages.get("account.avatar.title.img", user), avatar);
                    if (avatarCloudinary != null) {
                        user.avatar = avatarCloudinary;
                        user.save();
                        flash.success(Messages.get("account.avatar.success"));
                    } else {
                        flash.error(Messages.get("account.avatar.error"));
                    }
                } else {
                    flash.error(Messages.get("account.avatar.ext.wrong"));
                }
            }
        }
    }

    /**
     * Redimensionne l'avatar
     * @param avatar L'avatar non redimensionné
     * @return L'avatar redimensionné
     */
    public static File resizeAvatar(File avatar){
        File avatarResized = avatar;
        int maxWidth = ConfigurationUtil.getIntValue("avatar.width.limit");
        int maxHeight = ConfigurationUtil.getIntValue("avatar.height.limit");
        Images.resize(avatar, avatarResized, maxWidth, maxHeight, true);
        return avatarResized;
    }

    /**
     * Redimmensionne une image automatiquement en fonction des paramètres de l'application
     * @param image
     * @return L'image redimenssionée
     */
    public static File autoResize(File image){
        if (ConfigurationUtil.getBoolValue("cloudinary.image.resize")) {
            int maxWidth = ConfigurationUtil.getIntValue("cloudinary.width.limit");
            int maxHeight = ConfigurationUtil.getIntValue("cloudinary.height.limit");
            return resize(image, maxWidth, maxHeight, true);
        }
        return image;
    }

    /**
     * Redimmensionne une image
     * @param image L'image a redimmensionnée
     * @param maxWidth La largeur max
     * @param maxHeight La hauetur max
     * @return L'image redimenssionnée
     */
    public static File resize(File image, int maxWidth, int maxHeight, boolean keepRatio){
        File imageResized = new File(image.getName());
        Images.resize(image, imageResized, maxWidth, maxHeight, keepRatio);
        return imageResized;
    }

    /**
     * Compresse une image
     * @param in L'image source
     * @param compression Le niveau de compression de 0.0F à 1.0F
     * @return L'image compressée
     */
    public static File compress(File in, float compression){
        File out = new File(in.getName());
        try {
            RenderedImage image = ImageIO.read(in);
            ImageWriter writer = null;
            Iterator iter = ImageIO.getImageWritersByFormatName("jpg");
            if(iter.hasNext()){
                writer = (ImageWriter) iter.next();
            }
            ImageOutputStream outStream = ImageIO.createImageOutputStream(out);
            writer.setOutput(outStream);
            ImageWriteParam iwparam = new ImageWriteParam(new Locale("fr"));
            iwparam.setCompressionMode(ImageWriteParam.MODE_DEFAULT) ;
            iwparam.setCompressionQuality(compression);
            writer.write(null, new IIOImage(image, null, null), iwparam);
            outStream.flush();
            writer.dispose();
            outStream.close();
            return out;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return in;
    }
}
