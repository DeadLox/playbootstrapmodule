package util;

import play.mvc.Http;
import play.mvc.Scope;

import java.util.Collection;

/**
 * Created by cleborgne on 16/02/2015.
 */
public class Pagination {
    private Http.Request request;
    private int currentPage;
    private int nbPage;
    private int limit;
    private int start;
    private int to;
    private long total;
    private String search;
    private Collection collection;

    public Pagination() {
        init();
    }

    public Pagination(Http.Request request) {
        this.request = request;
        this.limit = 10;
        this.currentPage = 1;
        this.search = "";
        init();
    }

    public Pagination(long total) {
        this.total = total;
    }

    public Pagination(Collection collection) {
        this.collection = collection;
    }

    /**
     * Initialise la pagination
     */
    private void init(){
        Scope.Params params = this.request.params;
        if (params != null) {
            if (params._contains("page")) {
                this.currentPage = Integer.parseInt(params.get("page"));
            }
            if (params._contains("limit")) {
                this.limit = Integer.parseInt(params.get("limit"));
            }
            if (params._contains("search")) {
                this.search = params.get("search");
            }
        }
        this.nbPage = (int) Math.ceil((double)this.total/(double)this.limit);
        // Si on dépasse les limites
        if (this.nbPage > 0 && this.currentPage > this.nbPage) {
            this.currentPage = this.nbPage;
        } else if (this.currentPage < 1) {
            this.currentPage = 1;
        }
        this.start = this.limit * (this.currentPage-1);
        this.to = this.start+this.limit;
    }

    /** Getters */
    public int getCurrentPage() {
        return currentPage;
    }

    public int getStart() {
        return start;
    }

    public int getTo() {
        return to;
    }

    public long getTotal() {
        return total;
    }

    public String getSearch() {
        return search;
    }

    public int getNbPage() {
        return nbPage;
    }

    public int getLimit() {
        return limit;
    }

    public Collection getCollection() {
        return collection;
    }

    /** Setters */
    public void setCollection(Collection collection) {
        this.collection = collection;
    }

    public void setTotal(long total) {
        this.total = total;
        init();
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setLimit(int limit){
        this.limit = limit;
    }

    public void setCurrentPage(int page){
        this.currentPage = page;
        init();
    }
}
