package util;

import com.google.common.io.Files;
import controllers.Application;
import interfaces.BootstrapConstants;
import models.Attempt;
import models.Utilisateur;
import org.apache.log4j.Logger;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import play.i18n.Messages;
import play.libs.Codec;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Router;
import play.mvc.Scope;

import java.io.File;
import java.util.*;

/**
 * Created by DeadLox on 30/05/2014.
 */
public class BootstrapUtil {
    /** Logger */
    private static Logger logger = Logger.getLogger(BootstrapUtil.class);

    /**
     * Néttoie une chaine
     * Supprime tous les tags HTML
     * @param text La chaine a néttoyer
     * @return La chaine néttoyée
     */
    public static String cleanHTMLString(String text){
        return text.replaceAll("\\<.*?>","");
    }

    /**
     * Contrôle les types que le rôle Contributeur peut voir
     * @param typeName
     * @return
     */
    public static boolean contributeurCanShowType(String typeName){
        List<String> typeCanShow = Arrays.asList("Articles", "Categorys", "Documents", "CloudinaryImages");
        if (typeCanShow.contains(typeName)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne TRUE si le code est valide
     * @return
     */
    public static void checkCodeValidity(Date dateCode){
        Interval interval = new Interval(dateCode.getTime(), new Date().getTime());
        if (interval.toDuration().toStandardHours().getHours() > ConfigurationUtil.getIntValue("application.code.validite")) {
            Scope.Flash.current().error(Messages.get("secure.reset.code.over"));
            Application.index();
        }
    }

    /**
     * Génère un code unique
     * @param user
     */
    public static void generateUniqueCode(Utilisateur user){
        user.code = Codec.UUID();
        user.codeDate = new Date();
    }

    /**
     * Vérifie si l'extension du fichier est présent dans la liste passée en paramètre
     * @param file Le fichier à vérifier
     * @param exts La liste des extensions en majuscule
     * @return <code>TRUE</code> si l'extension est dans la liste
     */
    public static boolean checkExtensionFile(File file, List<String> exts){
        String ext = Files.getFileExtension(file.getName());
        if (exts.contains(ext.toUpperCase())){
            return true;
        }
        return false;
    }

    /**
     * Vérifie le nombre de tentative
     * @param type Le type de soumission BootstrapConstants
     * @return boolean <code>TRUE</code> si le nombre de tentatives est inférieur au seuil configuré
     */
    public static boolean checkAttempt(String type){
        // Seulement si la vérification des tentatives est activée
        if (ConfigurationUtil.getBoolValue("application.form.attempt")) {
            cleanAttemptHistory();
            // Sauvegarde cette nouvelle tentative
            String ip = getRealIpFromHeroku();
            Date date = new Date();
            Attempt attempt = new Attempt(ip, date, type);
            attempt.save();
            // Vérifie qu'on ne dépasse pas le nombre maximum de tentatives
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.SECOND, -BootstrapConstants.ATTEMPT_MAX_TIME);
            long nbAttempt = Attempt.count("ip = ? AND type = ? AND date >= ?", ip, type, cal.getTime());
            if (nbAttempt <= BootstrapConstants.ATTEMP_MAX_TENTATIVE) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * Tente de récupérer l'ip du client
     * @return
     */
    public static String getRealIpFromHeroku(){
        Http.Request currentResquest = Http.Request.current.get();
        Map<String, Http.Header> headers = currentResquest.headers;
        if (headers.containsKey("x-forwarded-for")) {
            return headers.get("x-forwarded-for").value();
        }
        return currentResquest.get().remoteAddress;
    }

    /**
     * Supprime de l'historique les tentatives dont la date est supérieure à deux fois le delai de tentative max
     */
    private static void cleanAttemptHistory(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.SECOND, -BootstrapConstants.ATTEMPT_MAX_TIME * 2);
        Attempt.delete("date <= ?", cal.getTime());
    }

    /**
     * Retourne le texte d'une durée  à partir des secondes
     * @param seconds Les secondes à convertir
     * @return duration La durée convertie
     */
    public static String formatSeconds(long seconds){
        Duration duration = new Duration(seconds*1000);
        Period p = duration.toPeriod(PeriodType.yearDayTime());
        if (p.getHours() > 0) {
            return Messages.get("duration.heures", p.getHours(), p.getMinutes(), p.getSeconds());
        } else if (p.getMinutes() > 0) {
            if (p.getSeconds() > 0) {
                return Messages.get("duration.minutes.secondes", p.getMinutes(), p.getSeconds());
            } else {
                return Messages.get("duration.minutes", p.getMinutes());
            }
        } else {
            return Messages.get("duration.seconds", p.getSeconds());
        }
    }

    /**
     * Vérifie si c'est un hash MD5
     * @param s la chaine à tester
     * @return <code>TRUE</code> si c'est hashé en MD5
     */
    public static boolean isValidMD5(String s) {
        return s.matches("[a-fA-F0-9]{32}");
    }
}
