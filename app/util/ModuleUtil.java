package util;

import play.Play;
import play.i18n.Messages;

/**
 * Created by cleborgne on 02/06/2014.
 */
public class ModuleUtil {

    /**
     * Vérifie si Algolia Search est configuré
     * @return <code>TRUE</code> si configuré
     */
    public static boolean algoliaSearchEnable(){
        return ConfigurationUtil.getBoolValue("algolia.index.enable");
    }

    /**
     * Vérifie si Google Analytics est configuré
     * @return <code>TRUE</code> si configuré
     */
    public static boolean gaEnable(){
        return ConfigurationUtil.getBoolValue("google.analytics.enable");
    }

    /**
     * Vérifie si Embed est configuré
     * @return <code>TRUE</code> si configuré
     */
    public static boolean embedEnable(){
        return ConfigurationUtil.getBoolValue("embed.api.enable");
    }

    /**
     * Vérifie si Cloudinary est configuré
     * @return <code>TRUE</code> si configuré
     */
    public static boolean cloudinaryEnable(){
        return ConfigurationUtil.getBoolValue("cloudinary.enable");
    }

    /**
     * Vérifie si le SMTP est configuré
     * @return <code>TRUE</code> si configuré
     */
    public static boolean mailEnable(){
        String smtp = Play.configuration.getProperty("mail.smtp.host");
        if (smtp != null && !smtp.equals("127.0.0.1") && !smtp.equals("localhost")) {
            return true;
        }
        return false;
    }
}
