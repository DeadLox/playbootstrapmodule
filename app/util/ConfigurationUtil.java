package util;

import models.Configuration;
import models.Role;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe utilitaire pour les configurations
 * Created by DeadLox on 12/06/2014.
 */
public class ConfigurationUtil {

    /**
     * Retourne une propriété sous forme de tableau
     * @param key La clé
     * @return Le tableau de propriétés
     */
    public static List<String> getStringArrayValue(String key){
        Configuration conf = Configuration.find("byKey", key).first();
        List<String> array = new ArrayList<String>();
        if (conf != null) {
            String[] stringArray = conf.value.split(",");
            return Arrays.asList(stringArray);
        } else {
            return array;
        }
    }

    /**
     * Retourne une propriété au format String
     * @param key La clé
     * @return La valeur de la propriété
     */
    public static String getStringValue(String key){
        Configuration conf = Configuration.find("byKey", key).first();
        if (conf != null) {
            return conf.value;
        } else {
            return "";
        }
    }

    /**
     * Retourne une propriété Boolean
     * @param key La clé
     * @return La valeur de la propriété
     */
    public static boolean getBoolValue(String key){
        Configuration conf = Configuration.find("byKey", key).first();
        if (conf != null) {
            if (Boolean.parseBoolean(conf.value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retourne une propriété Long
     * @param key La clé
     * @return La valeur de la propriété
     */
    public static long getLongValue(String key){
        Configuration conf = Configuration.find("byKey", key).first();
        if (conf != null) {
            return Long.parseLong(conf.value);
        }
        return 0;
    }

    /**
     * Retourne une propriété int
     * @param key La clé
     * @return La valeur de la propriété
     */
    public static int getIntValue(String key){
        Configuration conf = Configuration.find("byKey", key).first();
        if (conf != null) {
            return Integer.parseInt(conf.value);
        }
        return 0;
    }

    /**
     * Retourne une propriété double
     * @param key La clé
     * @return La valeur de la propriété
     */
    public static double getDoubleValue(String key){
        Configuration conf = Configuration.find("byKey", key).first();
        if (conf != null) {
            return Double.parseDouble(conf.value);
        }
        return 0;
    }

    /**
     * Retourne une propriété double
     * @param key La clé
     * @return La valeur de la propriété
     */
    public static Role getRoleValue(String key){
        Configuration conf = Configuration.find("byKey", key).first();
        if (conf != null) {
            return conf.getRole();
        }
        return null;
    }
}
