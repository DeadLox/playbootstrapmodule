package util;

import play.Logger;
import play.i18n.Messages;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by cleborgne on 23/05/2014.
 */
public class Embed {

    /**
     * Retourne l'url sécurisée vers un contenu
     * Utilisatation de Embed.ly
     * @return L'url safe vers le contenu
     * @throws UnsupportedEncodingException
     */
    public static String getEmbedLink(String urlContent) {
        String encodedUrlContent = "";
        String safeUrl = "";
        try {
            encodedUrlContent = URLEncoder.encode(urlContent, "UTF-8");
            String apiUrl =  ConfigurationUtil.getStringValue("embed.api.url");
            String apiKey = ConfigurationUtil.getStringValue("embed.api.key");
            safeUrl = apiUrl;
            safeUrl += "url=" + encodedUrlContent;
            safeUrl +=  "&key=" + apiKey;
            Logger.warn("Url: " + safeUrl);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return safeUrl;
    }
}
