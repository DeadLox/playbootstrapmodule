package util;

import models.Etat;
import models.Role;
import models.Utilisateur;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cleborgne on 19/08/2014.
 */
public class NewsletterUtil {
    private static Logger logger = Logger.getLogger(NewsletterUtil.class);

    /* Types */
    public static String TYPE_USER = "user";
    public static String TYPE_ROLE = "role";
    public static String TYPE_ETAT = "etat";
    public static String TYPE_OPTION = "option";
    /* Options */
    public static String OPTION_ALL_USERS = "allUsers";
    public static String OPTION_ALL_ROLES = "allRoles";
    public static String OPTION_ALL_ETATS = "allEtats";

    /**
     * Extrait la liste des destinataires à partir des tags
     * @param to La liste des tags
     * @return La liste des utlisateurs destinataires
     */
    public static List<Utilisateur> getListTo(String[] to){
        List<Utilisateur> destinataires = new ArrayList<Utilisateur>();
        for (String itTo : to) {
            String[] item = itTo.split(",");
            String value = item[0];
            String type = item[1];
            if (type.equals(TYPE_USER)) {
                destinataires.add(getUtilisateur(Long.parseLong(value)));
            } else if (type.equals(TYPE_ROLE)) {
                Role role = Role.findById(Long.parseLong(value));
                destinataires.addAll(role.utilisateur);
            } else if (type.equals(TYPE_ETAT)) {
                Etat etat = Etat.findById(Long.parseLong(value));
                destinataires.addAll(etat.utilisateurs);
            } else if (type.equals(TYPE_OPTION)) {
                if (value.equals(OPTION_ALL_USERS)) {
                    List<Utilisateur> allUsers = Utilisateur.findAll();
                    destinataires.addAll(allUsers);
                } else if (value.equals(OPTION_ALL_ROLES)) {
                    List<Role> roles = Role.findAll();
                    for (Role role : roles) {
                        destinataires.addAll(role.utilisateur);
                    }
                } else if (value.equals(OPTION_ALL_ETATS)) {
                    List<Etat> etats = Etat.findAll();
                    for (Etat etat : etats) {
                        destinataires.addAll(etat.utilisateurs);
                    }
                }
            }
        }
        return destinataires;
    }

    /**
     * Retourne un utilisateur à partir de son ID
     * @param id Son ID
     * @return L'utilisateur
     */
    private static Utilisateur getUtilisateur(long id){
        return Utilisateur.findById(id);
    }
}
