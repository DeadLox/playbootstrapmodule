package util;

import com.google.common.reflect.ClassPath;
import models.Data;
import org.json.JSONObject;
import play.Play;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cleborgne on 05/06/2014.
 */
public class IndexUtil {
    /**
     * Nom du package à surveiller
     */
    private static String PACKAGE = "models";
    /**
     * Nom de la méthode à surveiller
     */
    private static String METHOD = "toJson";

    /**
     * Retourne la liste des contenus de la classe au format JSON
     * @param clazz La classe à indexer
     * @return boolean <code>TRUE</code> si l'indexation s'est déroulé correctement
     */
    public static List<JSONObject> getJsonFromClass(Class clazz){
        List<JSONObject> listJson = new ArrayList<JSONObject>();
        try {
            Method methodFindAll = clazz.getMethod("findAll");
            try {
                List<Data> listObject = (List<Data>) methodFindAll.invoke(null);

                Method methodToJson = clazz.getMethod(METHOD);
                for (Data data : listObject) {
                    JSONObject dataJson = (JSONObject) methodToJson.invoke(data, null);
                    play.Logger.warn("" + dataJson);
                    listJson.add(dataJson);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return listJson;
    }

    /**
     * retourne la liste des classes du package Models possèdant la méthode toJson
     * @return List<Class> La liste des classes possèdant la méthode toJson
     */
    public static List<Class> getAllJsonClass(){
        List<Class> classes = getModelsClass();
        return getJsonClass(classes);
    }

    /**
     * Retourne toutes les classes du package Models
     * @return La liste des classes du package Models
     */
    private static List<Class> getModelsClass(){
        List<Class> models = new ArrayList<Class>();
        try {
            ClassPath classPath = ClassPath.from(Play.classloader);
            List<Class> classes = Play.classloader.getAllClasses();
            for (Class clazz : classes) {
                if (clazz.getName() != null && clazz.getName().startsWith(PACKAGE)) {
                    models.add(clazz);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return models;
    }

    /**
     * Retourne les classes qui possèdent la méthode toJson
     * @param classes la liste de classes à tester
     * @return List<Class> la liste des classes possèdant la méthode toJson
     */
    private static List<Class> getJsonClass(List<Class> classes){
        List<Class> jsonClass = new ArrayList<Class>();
        for (Class clazz : classes) {
            try {
                Method method = clazz.getMethod(METHOD);
                jsonClass.add(clazz);
            } catch (NoSuchMethodException e) {
            }
        }
        return jsonClass;
    }
}
