package util;

import org.apache.commons.lang.time.DateUtils;

import java.util.Date;

/**
 * Created by cleborgne on 07/08/2014.
 */
public class DateUtil {

    /**
     * Vérifie si les deux dates correspondent au même jour
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isSameDay(Date date1, Date date2){
        if (date1 != null && date2 != null) {
            return DateUtils.isSameDay(date1, date2);
        }
        return false;
    }
}
