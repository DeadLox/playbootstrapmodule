package util;

import com.algolia.search.saas.APIClient;
import com.algolia.search.saas.AlgoliaException;
import com.algolia.search.saas.Index;
import com.algolia.search.saas.Query;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import play.Play;
import play.i18n.Messages;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cleborgne on 04/06/2014.
 */
public class AlgoliaUtil {
    private static Logger logger = Logger.getLogger(AlgoliaUtil.class);

    /**
     * Retourne l'API Algolia search
     * @return
     */
    public static APIClient getAPI(){
        String appId = ConfigurationUtil.getStringValue("algolia.app.id");
        String apiKey = ConfigurationUtil.getStringValue("algolia.api.key");
        return new APIClient(appId, apiKey);
    }

    /**
     * Permet de tester l'indexation avec Algolia Search
     * @return boolean <code>TRUE</code> Si le test s'est effectué correctement
     */
    public static boolean test(){
        APIClient client = getAPI();
        Index index = client.initIndex("test");
        try {
            // Efface tout
            index.clearIndex();
            // On insère deux objets
            JSONObject first = new JSONObject();
            first.put("firstname", "Jimmie");
            first.put("lastname", "Barninger");
            first.put("followers", 93);
            first.put("company", "California Paint");
            JSONObject second = new JSONObject();
            second.put("firstname", "Warren");
            second.put("lastname", "Speach");
            second.put("followers", 42);
            second.put("company", "Norwalk Crmc");
            List<JSONObject> jsonList = new ArrayList<JSONObject>();
            jsonList.add(first);
            jsonList.add(second);
            index.addObjects(jsonList);

            // On effectue une recherche
            JSONObject result = index.search(new Query("jimmie"));
            if (result.has("nbHits")) {
                client.deleteIndex("test");
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (AlgoliaException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Indexe tous les types
     * @return boolean <code>TRUE</code> si l'indexation s'est déroulé correctement
     */
    public static boolean indexAllType(){
        logger.info("Reindexation de tous les types");
        List<Class> classes = IndexUtil.getAllJsonClass();
        for (Class clazz : classes) {
            boolean indexedClass = indexType(clazz);
            if (!indexedClass) {
                return false;
            }
        }
        return true;
    }

    /**
     * Indexe un type de contenu
     * @param clazz La classe à indexer
     * @return <code>TRUE</code> si l'indexation s'est déroulée correctement
     */
    public static boolean indexType(String clazz){
        boolean indexed = false;
        try {
            Class modelClazz = Class.forName("models."+clazz);
            indexed = indexType(modelClazz);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return indexed;
    }

    /**
     * Indexe un type de contenu
     * @param clazz La classe à indexer
     * @return <code>TRUE</code> si l'indexation s'est déroulée correctement
     */
    public static boolean indexType(Class clazz){
        logger.info("Reindexation du type " + clazz);
        boolean indexed = false;
        List<JSONObject> listClassJson = IndexUtil.getJsonFromClass(clazz);
        APIClient client = getAPI();
        Index index = client.initIndex(clazz.getSimpleName());
        try {
            // Indexe tous les objets du type
            index.addObjects(listClassJson);
            indexed = true;
        } catch (AlgoliaException e) {
            e.printStackTrace();
        }
        return indexed;
    }

    /**
     * Retourne le résultat de la méthode CountAll (JPA) d'une classe
     * @param clazz La classe à tester
     * @return nb le résultat de la méthode CountAll
     */
    public static long countDataFromClass(Class clazz){
        long nb = 0;
        try {
            Method count = clazz.getMethod("count");
            try {
                Object result = count.invoke(null);
                if (result != null) {
                    nb = Long.parseLong(result.toString());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return nb;
    }

    /**
     * Vérifie si l'indéxation de la classe est activé
     * @param clazz La classe à tester
     * @return <code>TRUE</code> si l'indexation de la classe est activée
     */
    public static boolean indexEnable(Class clazz){
        return indexEnable(clazz.getSimpleName());
    }

    /**
     * Vérifie si l'indéxation de la classe est activé
     * @param clazz Le nom de la classe à tester
     * @return <code>TRUE</code> si l'indexation de la classe est activée
     */
    public static boolean indexEnable(String clazz){
        String conf = Play.configuration.getProperty("algolia.index." + clazz);
        if (conf != null && Boolean.parseBoolean(conf)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne le nombre de résulats dans les index
     * @return nb
     */
    public static List<Map<String, String>> getIndexInfo(){
        APIClient client = getAPI();
        List<Map<String,String>> listIndex = new ArrayList<Map<String, String>>();
        try {
            JSONObject jsonResult = client.listIndexes();
            if (jsonResult.has("items")) {
                try {
                    JSONArray items = jsonResult.getJSONArray("items");
                    for (int i=0; i < items.length(); i++) {
                        JSONObject index = (JSONObject) items.get(i);
                        if (index != null) {
                            Map<String, String> mapIndex = new HashMap<String, String>();
                            mapIndex.put("name", index.getString("name"));
                            mapIndex.put("entries", ""+index.getLong("entries"));
                            mapIndex.put("createdAt", index.getString("createdAt"));
                            mapIndex.put("updatedAt", index.getString("updatedAt"));
                            mapIndex.put("pendingTask", "" + index.getBoolean("pendingTask"));
                            listIndex.add(mapIndex);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (AlgoliaException e) {
            e.printStackTrace();
        }
        return listIndex;
    }

    /**
     * Effectue une recherche sur un Index
     * @param recherche La recherche
     * @param indexName Le nom de l'index
     */
    public static List<Map<String,String>> doSearch(String recherche, String indexName){
        APIClient client = getAPI();
        List<Map<String,String>> listHits = new ArrayList<Map<String, String>>();
        Index index = client.initIndex(indexName);
        if (index != null) {
            try {
                JSONObject resultQuery = index.search(new Query(recherche));
                if (resultQuery != null && resultQuery.has("hits")) {
                    logger.warn(resultQuery);
                    try {
                        JSONArray hits = resultQuery.getJSONArray("hits");
                        for (int i=0; i < hits.length(); i++) {
                            JSONObject hit = (JSONObject) hits.get(i);
                            if (hits != null) {
                                Map<String, String> mapHit = new HashMap<String, String>();
                                mapHit.put("email", hit.getString("email"));
                                listHits.add(mapHit);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (AlgoliaException e) {
                e.printStackTrace();
            }
        }
        return listHits;
    }
}
