package util;

import com.cloudinary.Api;
import com.cloudinary.Cloudinary;
import com.google.gson.JsonObject;
import models.CloudinaryFile;
import models.CloudinaryImage;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import play.i18n.Messages;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.util.*;

/**
 * Classe permettant de communiquer avec le service Cloudinary
 * Created by cleborgne on 26/05/2014.
 */
public class CloudinaryUtil {
    private static Logger logger = Logger.getLogger(CloudinaryImage.class);

    public static String SUCCESS = "ok";
    public static String NOT_FOUND = "not found";

    // Type de transformation
    public static String TYPE_FIT = "fit";
    public static String TYPE_FILL = "fill";
    public static String TYPE_CROP = "crop";

    /**
     * Recupère le singleton Cloudinary
     * @return Cloudinary l'instance
     */
    public static Cloudinary getCloudinary(){
        Cloudinary cloudinary = new Cloudinary(Cloudinary.asMap(
                "cloud_name", ConfigurationUtil.getStringValue("cloudinary.cloud.name"),
                "api_key", ConfigurationUtil.getStringValue("cloudinary.api.key"),
                "api_secret", ConfigurationUtil.getStringValue("cloudinary.api.secret")));
        return cloudinary;
    }

    /**
     * Upload un array d'images
     * @param files Le tableau de fichier
     * @return La liste de CloudinaryImage
     */
    public static List<CloudinaryImage> uploadImages(File[] files){
        List<CloudinaryImage> cloudinaryImages = new ArrayList<CloudinaryImage>();
        if (files != null) {
            for (File file : files) {
                cloudinaryImages.add(uploadImage(file));
            }
        }
        return cloudinaryImages;
    }

    /**
     * Crée une image Cloudinary
     * @param image Le fichier
     * @return L'object CloudinaryImage
     */
    public static CloudinaryImage uploadImage(File image){
        return uploadImage(image.getName(), image);
    }

    /**
     * Upload une image sur Cloudinary
     * @param image L'image à uploader
     */
    public static CloudinaryImage uploadImage(String libelle, File image){
        Cloudinary cloudinary = getCloudinary();
        CloudinaryImage cloudinaryImage = null;

        // Redimmensionne la photo si c'est activé
        image = ImageUtil.autoResize(image);

        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("angle", "90");
            Map uploadResult = cloudinary.uploader().upload(image, params);
            if (uploadResult != null) {
                cloudinaryImage = new CloudinaryImage();
                // Si aucun libellé n'est précisé, on reprend le nom du fichier
                if (libelle == null || libelle.equals("")) {
                    libelle = image.getName();
                }
                cloudinaryImage.libelle = libelle;
                cloudinaryImage.bytes = (Long) uploadResult.get("bytes");
                cloudinaryImage.etag = (String) uploadResult.get("etag");
                cloudinaryImage.width = (Long) uploadResult.get("width");
                cloudinaryImage.public_id = (String) uploadResult.get("public_id");
                cloudinaryImage.format = (String) uploadResult.get("format");
                cloudinaryImage.type = (String) uploadResult.get("type");
                cloudinaryImage.url = (String) uploadResult.get("url");
                cloudinaryImage.version = (Long) uploadResult.get("version");
                cloudinaryImage.height = (Long) uploadResult.get("height");
                cloudinaryImage.ressource_type = (String) uploadResult.get("resource_type");
                cloudinaryImage.signature = (String) uploadResult.get("signature");
                cloudinaryImage.secure_url = (String) uploadResult.get("secure_url");
                cloudinaryImage.save();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cloudinaryImage;
    }

    /**
     * Upload une image sur Cloudinary
     * @param file Le fichier à uploader
     */
    public static CloudinaryFile uploadRaw(String libelle, File file){
        Cloudinary cloudinary = getCloudinary();
        CloudinaryFile CloudinaryFile = null;

        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("resource_type", "raw");
            Map uploadResult = cloudinary.uploader().upload(file, params);
            if (uploadResult != null) {
                CloudinaryFile = new CloudinaryFile();
                // Si aucun libellé n'est précisé, on reprend le nom du fichier
                if (libelle == null || libelle.equals("")) {
                    libelle = file.getName();
                }
                CloudinaryFile.libelle = libelle;
                CloudinaryFile.bytes = (Long) uploadResult.get("bytes");
                CloudinaryFile.etag = (String) uploadResult.get("etag");
                CloudinaryFile.public_id = (String) uploadResult.get("public_id");
                CloudinaryFile.format = FilenameUtils.getExtension(file.getName());
                CloudinaryFile.type = (String) uploadResult.get("type");
                CloudinaryFile.url = (String) uploadResult.get("url");
                CloudinaryFile.version = (Long) uploadResult.get("version");
                CloudinaryFile.ressource_type = (String) uploadResult.get("resource_type");
                CloudinaryFile.signature = (String) uploadResult.get("signature");
                CloudinaryFile.secure_url = (String) uploadResult.get("secure_url");
                CloudinaryFile.save();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return CloudinaryFile;
    }

    /**
     * Supprime un fichier Cloudinary grâce à son ID
     * @param cloudinaryFile
     */
    public static String delete(CloudinaryFile cloudinaryFile){
        Cloudinary cloudinary = getCloudinary();
        try {
            Map<String, Object> params = new HashMap<String, Object>();
            if (cloudinaryFile instanceof CloudinaryImage) {
            } else {
                params.put("resource_type", "raw");
            }
            Map<String, Object> result = cloudinary.uploader().destroy(cloudinaryFile.public_id, params);
            if (result.containsKey("result") && result.get("result").equals(SUCCESS)) {
                cloudinaryFile.delete();
                return SUCCESS;
            } else if (result.containsKey("result") && result.get("result").equals(NOT_FOUND)) {
                cloudinaryFile.delete();
                return SUCCESS;
            } else {
                return (String) result.get("result");
            }
        } catch (ConnectException e) {
            e.printStackTrace();
            return Messages.get("connection.exception", "Cloudinary");
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
