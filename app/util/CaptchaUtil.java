package util;

import com.google.gson.JsonElement;
import controllers.Secure;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import play.Play;
import play.classloading.enhancers.ControllersEnhancer;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.libs.WS;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Scope;
import play.mvc.With;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe utilitaire pour la gestion du Captcha
 *
 * Created by cleborgne on 04/12/2014.
 */
public class CaptchaUtil {
    /** Logger */
    private static Logger logger = Logger.getLogger(CaptchaUtil.class);

    /** URL */
    private static String GOOGLE_CAPTCHA_URL_VERIFY     = "https://www.google.com/recaptcha/api/siteverify";

    /** Form param */
    private static String FORM_RECAPTCHA_RESPONSE       = "g-recaptcha-response";

    /** Configurations key */
    private static String CONF_CAPTCHA_ENABLE           = "captcha.enable";
    private static String CONF_CAPTCHA_SITE_KEY         = "captcha.site.key";
    private static String CONF_CAPTCHA_SECRET_KEY       = "captcha.secret.key";

    /** JSON Key */
    private static String JSON_KEY_SUCCESS              = "success";
    private static String JSON_KEY_ERROR                = "error-codes";

    /** Error codes */
    private static String ERROR_MISSING_INPUT_SECRET    = "missing-input-secret";
    private static String ERROR_INVALID_INPUT_SECRET    = "invalid-input-secret";
    private static String ERROR_MISSING_INPUT_RESPONSE  = "missing-input-response";
    private static String ERROR_INVALID_INPUT_RESPONSE  = "invalid-input-response";

    /**
     * Vérifie si le captcha est activé sur le site
     * @return <code>TRUE</code> si le module Captcha est activé
     */
    public static boolean captchaEnable(){
        return ConfigurationUtil.getBoolValue(CONF_CAPTCHA_ENABLE);
    }

    /**
     * Retourne la clé du site stockée dans les configurations
     * @return La clé du site
     */
    public static String getSiteKey(){
        return ConfigurationUtil.getStringValue(CONF_CAPTCHA_SITE_KEY);
    }

    /**
     * Vérifie le captcha si celui-ci est activé sur le site
     * @return <code>TRUE</code> si l'identification est validée par Google
     */
    public static boolean checkCaptcha(){
        if (captchaEnable()) {
            String secretKey = ConfigurationUtil.getStringValue(CONF_CAPTCHA_SECRET_KEY);
            if (secretKey != null) {
                String gRecaptchaResponse = Http.Request.current().get().params.get(FORM_RECAPTCHA_RESPONSE);
                Map<String, String> params = new HashMap<String, String>();
                params.put("secret", secretKey);
                params.put("response", gRecaptchaResponse);
                params.put("remoteip", BootstrapUtil.getRealIpFromHeroku());
                WS.HttpResponse res = WS.url(GOOGLE_CAPTCHA_URL_VERIFY).setParameters(params).get();
                if (res.success() && res.getJson() != null) {
                    try {
                        boolean captchaValid = false;
                        JSONObject jsonResult = new JSONObject(res.getJson().toString());
                        if (jsonResult.has(JSON_KEY_SUCCESS) && jsonResult.getBoolean(JSON_KEY_SUCCESS)) {
                            captchaValid = true;
                        } else {
                            Validation.current().addError("captcha", "captcha.error");
                        }
                        if (jsonResult.has(JSON_KEY_ERROR)) {
                            JSONArray errorCodes = jsonResult.getJSONArray(JSON_KEY_ERROR);
                            for (int i=0; i<errorCodes.length(); i++) {
                                String errorCode = (String) errorCodes.get(i);
                                if (errorCode.equals(ERROR_MISSING_INPUT_SECRET)) {
                                    logger.warn(Messages.get("captcha.error.missing.input.secret"));
                                } else if (errorCode.equals(ERROR_INVALID_INPUT_SECRET)) {
                                    logger.warn(Messages.get("captcha.error.invalid.input.secret"));
                                } else if (errorCode.equals(ERROR_MISSING_INPUT_RESPONSE)) {
                                    logger.warn(Messages.get("captcha.error.missing.input.response"));
                                } else if (errorCode.equals(ERROR_INVALID_INPUT_RESPONSE)) {
                                    logger.warn(Messages.get("captcha.error.invalid.input.response"));
                                }
                            }
                        }
                        return captchaValid;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }
}
