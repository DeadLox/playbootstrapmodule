package util;

import controllers.Application;
import controllers.Security;
import models.Role;
import models.Utilisateur;
import play.i18n.Messages;

/**
 * Created by DeadLox on 26/03/15.
 */
public class BoardUtil {

    /**
     * Vérifie que l'utilisateur peut accèder au Forum
     * @return <code>TRUE</code> si l'utilisateur peut accèder au Forum
     */
    public static boolean canAccessForum(){
        Utilisateur user = Security.getLoggedMember();
        Role roleUser = user.getHighterRole();
        Role roleMin = util.ConfigurationUtil.getRoleValue("forum.role.min");
        if (roleMin == null || roleUser.level >= roleMin.level) {
            return true;
        }
        return false;
    }
}
