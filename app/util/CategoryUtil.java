package util;

import models.Category;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DeadLox on 23/08/2014.
 */
public class CategoryUtil {

    public static JSONObject fetchCategory(Category category){
        JSONObject jsonObject = new JSONObject();

        try {
            // Categorie parente
            if (category.parent != null) {
                jsonObject.put("parent", category.parent.toJSON(false));
            }

            // Categorie courante
            jsonObject.put("category", category.toJSON(false));

            // Catégories
            jsonObject.put("childrenSet", category.childrenSetToJSON(false));

            // Cloudinary Images
            jsonObject.put("images", category.imagesToJSON());

            // Cloudinary Files
            jsonObject.put("files", category.filesToJSON());

            // Documents
            jsonObject.put("documents", category.documentsToJSON());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
