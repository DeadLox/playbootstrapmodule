package util;

/**
 * Created by cleborgne on 03/02/2015.
 */
public class KimonoLabsUtil extends ServiceUtil {

    @Override
    public boolean isEnable() {
        if (ConfigurationUtil.getStringValue("algolia.index.enable").length() > 0) {
            return true;
        }
        return false;
    }
}
