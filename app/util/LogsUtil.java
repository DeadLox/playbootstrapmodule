package util;

import models.Attempt;
import models.Log;

import java.util.Date;

/**
 * Created by cleborgne on 09/02/2015.
 */
public class LogsUtil extends ServiceUtil {

    @Override
    public boolean isEnable() {
        return true;
    }

    /**
     * Ajoute un log
     * @param type Le type du log
     * @param sousType Le sous-type du log
     * @param destinataires La liste des destinataires de l'email
     * @param sujet Le sujet du mail
     * @param message Le message du mail
     */
    public static void addLog(String type, String sousType, String destinataires, String sujet, String message){
        Log log = new Log();
        log.type = type;
        log.sousType = sousType;
        log.destinataires = destinataires;
        log.sujet = sujet;
        log.message = message;
        log.save();
    }
}
