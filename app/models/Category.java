package models;

import controllers.CRUD;
import controllers.Security;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import play.data.validation.Required;
import play.db.jpa.Model;
import util.CloudinaryUtil;

import javax.persistence.*;
import java.util.*;

/**
 * Created by DeadLox on 15/12/2013.
 */
@Entity
public class Category extends Data implements Comparator<Category>, Comparable<Category> {

    @Required
    public String title;
    public String description;
    @OneToOne(optional=true)
    public Category parent;
    @OneToMany(mappedBy = "parent")
    @CRUD.Hidden
    @OrderBy("ordre DESC, title ASC")
    public Set<Category> childrenSet = new TreeSet<Category>();
    @OneToMany(mappedBy = "category")
    @CRUD.Hidden
    public Set<Document> documents = new TreeSet<Document>();
    @OneToMany(mappedBy = "category")
    @CRUD.Hidden
    public Set<CloudinaryFile> cloudinaryFiles = new TreeSet<CloudinaryFile>();
    @OneToMany(mappedBy = "category")
    @CRUD.Hidden
    public Set<Article> articles = new TreeSet<Article>();
    @Required
    @Column(columnDefinition = "smallint DEFAULT '0'")
    public int ordre = 0;

    /** Droits d'accès */
    @ManyToOne
    public Role viewableBy;

    /**
     * Vérifie si l'utilisateur à le rôle suffisant pour voir le forum
     * @param user L'utilisateur
     * @return <code>TRUE</code> si l'utilisateur a un rôle suffisant pour voir le Forum
     */
    public boolean canView(Utilisateur user){
        return (this.viewableBy.level <= user.getHighterRole().level);
    }

    public void deleteAllContent(){
        for (Category category : this.childrenSet) {
            category.deleteAllContent();
            category.delete();
        }
        this.childrenSet = null;
        for (Article article : this.articles) {
            article.delete();
        }
        this.articles = null;
        for (CloudinaryFile cloudinaryFile : this.cloudinaryFiles) {
            CloudinaryUtil.delete(cloudinaryFile);
        }
        this.cloudinaryFiles = null;
        for (Document document : this.documents) {
            document.delete();
        }
        this.documents = null;
    }

    public boolean isInCategoryByName(String categoryName, boolean deep) {
        Category category = Category.find("byTitle", categoryName).first();
        if (category != null) {
            return isInCategory(category, deep);
        }
        return false;
    }

    public boolean isInCategory(Category categoryToCheck, boolean deep) {
        if (this.equals(categoryToCheck)) {
            return true;
        } else {
            if (deep) {
                for (Category categoryChild : this.childrenSet) {
                    return categoryChild.isInCategory(categoryToCheck, true);
                }
            }
            return false;
        }
    }

    /**
     * Retourne la liste des catégories enfants visibles par l'utilisateur connecté
     * @return La liste des catégories visibles
     */
    public List<Category> getViewableChildrenSet(){
        Utilisateur loggedMember = Security.getLoggedMember();
        Role highterRole = loggedMember.getHighterRole();
        // Ne fonctionne pas en une seule requête avec un OR ...
        // Première requête sur les viewableBy NULL
        List<Category> childrenViewableSet1 = Category.find("parent = ? AND viewableBy = NULL", this).fetch();
        // Deuxième requête en testant le rôle
        List<Category> childrenViewableSet2 = Category.find("parent = ? AND viewableBy.level <= ?", this, highterRole.level).fetch();
        // On fusionne les deux liste
        List<Category> childrenViewableSet = new ArrayList<Category>();
        childrenViewableSet.addAll(childrenViewableSet1);
        childrenViewableSet.addAll(childrenViewableSet2);
        Collections.sort(childrenViewableSet);
        return childrenViewableSet;
    }

    /**
     * Convertit la liste des images de la catégorie en tableau JSON
     * @return Le tableau d'image au format JSON
     */
    public JSONArray imagesToJSON(){
        JSONArray jsonArray = new JSONArray();
        for (CloudinaryFile cloudinaryFile : this.cloudinaryFiles) {
            if (cloudinaryFile instanceof CloudinaryImage) {
                jsonArray.put(((CloudinaryImage) cloudinaryFile).toJSON());
            }
        }
        return jsonArray;
    }

    /**
     * Convertit la liste des fichiers de la catégorie en tableau JSON
     * @return Le tableau de fichier au format JSON
     */
    public JSONArray filesToJSON(){
        JSONArray jsonArray = new JSONArray();
        for (CloudinaryFile cloudinaryFile : this.cloudinaryFiles) {
            if (cloudinaryFile instanceof CloudinaryImage) {
            } else {
                jsonArray.put(cloudinaryFile.toJSON());
            }
        }
        return jsonArray;
    }

    /**
     * Convertit la liste des documents de la catégorie en tableau JSON
     * @return Le tableau de documents au format JSON
     */
    public JSONArray documentsToJSON(){
        JSONArray jsonArray = new JSONArray();
        for (Document document : this.documents) {
            jsonArray.put(document.toJSON());
        }
        return jsonArray;
    }

    /**
     * Convertit la liste des catgories enfants en tableau JSON
     * @param recursive
     * @return Le tableau de catégorie JSON
     */
    public JSONArray childrenSetToJSON(boolean recursive){
        JSONArray jsonArray = new JSONArray();
        Set<Category> childrens = this.childrenSet;
        for (Category children : childrens) {
            jsonArray.put(children.toJSON(recursive));
        }
        return jsonArray;
    }

    /**
     * Convertit la catégorie en JSON
     * @return Un objet JSON
     */
    public JSONObject toJSON(boolean recursive){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", this.id);
            jsonObject.put("title", this.title);
            jsonObject.put("description", this.description);
            if (this.parent != null) {
                jsonObject.put("parent", this.parent.toJSON(false));
            }
            if (recursive) {
                jsonObject.put("childrenSet", this.childrenSetToJSON(recursive));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String toString(){
        return this.title;
    }

    @Override
    public int compare(Category c1, Category c2) {
        Integer ordre1 = c1.ordre;
        Integer ordre2 = c2.ordre;
        if (ordre1.compareTo(ordre2) == 0) {
            String titreC1 = c1.title;
            String titreC2 = c2.title;
            return titreC1.compareTo(titreC2);
        }
        return ordre1.compareTo(ordre2);
    }

    @Override
    public int compareTo(Category category) {
        Integer ordre1 = this.ordre;
        Integer ordre2 = category.ordre;
        if (ordre1.compareTo(ordre2) == 0) {
            String titreC1 = this.title;
            String titreC2 = category.title;
            return titreC1.compareTo(titreC2);
        }
        return ordre1.compareTo(ordre2);
    }
}
