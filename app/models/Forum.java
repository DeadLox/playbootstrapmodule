package models;

import controllers.CRUD;
import controllers.Security;
import play.data.validation.Required;
import play.db.jpa.JPA;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Query;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by cleborgne on 22/01/2015.
 */
@Entity
public class Forum extends DataForum {
    @Required
    public String titre;
    public String description;
    @Required
    @OneToOne
    @CRUD.ExtraData("Forum")
    public Category category;
    @OneToMany(mappedBy = "forum")
    @CRUD.Hidden
    public Set<Topic> topics = new TreeSet<Topic>();

    /** Statistiques */
    @CRUD.Hidden
    @OneToOne
    public Topic topic;
    @CRUD.Hidden
    @OneToOne
    public Post lastPost;
    @CRUD.Hidden
    public int nbPosts = 0;

    public Forum() {
    }

    /**
     * Met à jour les statistiques d'un forum
     * @param post Le dernier post créé
     */
    public void updateStat(Post post){
        this.nbPosts++;
        this.lastPost = post;
        this.save();
    }

    /**
     * Retourne la liste des forums appartenant à cette Categorie
     * @param category La catégorie
     * @return La liste des forums
     */
    public static List<Forum> getForumListByCat(Category category){
        Utilisateur loggedMember = Security.getLoggedMember();
        Role highterUserRole = loggedMember.getHighterRole();
        Query query = JPA.em().createNativeQuery("SELECT * \n" +
                "FROM Forum f \n" +
                "LEFT JOIN role r \n" +
                "ON r.id = f.viewableby_id  \n" +
                "WHERE f.category_id = :category_id \n" +
                "AND (r.level IS NULL OR r.level <= :level) \n" +
                "ORDER BY ordre \n" +
                "DESC, titre ASC",
                Forum.class);
        query.setParameter("category_id", category.id);
        query.setParameter("level", highterUserRole.level);
        return (List<Forum>) query.getResultList();
    }

    public String toString(){
        return this.titre;
    }


}
