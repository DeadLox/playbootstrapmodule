package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by cleborgne on 06/06/2014.
 */
@Entity
public class Attempt extends Model {
    public String ip;
    public Date date;
    public String type;

    public Attempt(String ip, Date date, String type){
        this.ip     = ip;
        this.date   = date;
        this.type   = type;
    }

    public String toString(){
        return this.type+"|"+this.ip+"|"+this.date;
    }
}
