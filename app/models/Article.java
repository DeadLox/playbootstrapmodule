package models;

import controllers.CRUD;
import org.hibernate.annotations.Type;
import play.data.validation.MaxSize;
import play.data.validation.Required;

import javax.persistence.*;

/**
 * Created by DeadLox on 28/10/14.
 */
@Entity
public class Article extends Publication implements Comparable<Article> {
    @Required
    public String titre;
    @Required
    @CRUD.Wysiwyg(toolbar = "simple")
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    public String contenu;
    @ManyToOne(cascade= CascadeType.PERSIST)
    public Category category;
    @OneToOne
    public Role roleVisibilite;

    public String toString(){
        return this.titre;
    }

    @Override
    public int compareTo(Article article) {
        return this.dateUpdate.compareTo(article.dateUpdate);
    }
}
