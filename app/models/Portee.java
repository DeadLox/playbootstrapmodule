package models;

import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;

/**
 * Created by DeadLox on 14/06/2014.
 */
@Entity
public class Portee extends Model implements Comparable<Portee> {
    @Required
    public String libelle;

    public String toString(){
        return this.libelle;
    }

    @Override
    public int compareTo(Portee o) {
        return this.libelle.compareTo(o.libelle);
    }
}
