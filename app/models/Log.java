package models;

import play.data.validation.Required;

import javax.persistence.Entity;

/**
 * Created by cleborgne on 09/02/2015.
 */
@Entity
public class Log extends Data {
    @Required
    public String type;
    @Required
    public String sousType;
    public String destinataires;
    public String sujet;
    public String message;

    public String toString(){
        return type+" - "+sujet;
    }
}
