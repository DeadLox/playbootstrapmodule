package models;

import controllers.CRUD;
import controllers.Security;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 * Created by cleborgne on 04/11/2014.
 */
@MappedSuperclass
public abstract class Publication extends Data {
    @CRUD.Hidden
    @ManyToOne
    public Utilisateur auteur;
}
