package models;

import controllers.CRUD;
import org.json.JSONException;
import org.json.JSONObject;
import play.Logger;
import play.data.validation.Required;
import play.i18n.Messages;
import util.CloudinaryUtil;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

/**
 * Created by cleborgne on 26/05/2014.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class CloudinaryFile extends Data {
    public String libelle;
    @CRUD.Hidden
    public long bytes;
    @CRUD.Hidden
    public String etag;
    @CRUD.Hidden
    @Required
    public String public_id;
    @CRUD.Hidden
    public String format;
    @CRUD.Hidden
    public String type;
    @Required
    public String url;
    @CRUD.Hidden
    public long version;
    @CRUD.Hidden
    public String ressource_type;
    @CRUD.Hidden
    public String signature;
    @CRUD.Hidden
    public String secure_url;
    @ManyToOne
    public Category category;
    @CRUD.Hidden
    @ManyToOne
    public Utilisateur auteur;

    public JSONObject toJSON(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("libelle", this.libelle);
            jsonObject.put("url", this.url);
            jsonObject.put("format", this.format);
            jsonObject.put("category", this.category.toJSON(false));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String toString(){
        return this.libelle;
    }
}
