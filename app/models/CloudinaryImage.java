package models;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import controllers.CRUD;
import org.json.JSONException;
import org.json.JSONObject;
import play.Logger;
import play.data.validation.Required;
import play.i18n.Messages;
import util.CloudinaryUtil;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by cleborgne on 26/05/2014.
 */
@Entity
public class CloudinaryImage extends CloudinaryFile {
    @CRUD.Hidden
    public long width;
    @CRUD.Hidden
    public long height;

    /**
     * Retourne une balise image avec l'image retaillée
     * @param width
     * @param height
     * @param type
     * @return
     */
    public String resize(int width, int height, String type){
        Cloudinary cloudinary = CloudinaryUtil.getCloudinary();
        String url = null;
        try {
            url = cloudinary.url().transformation(new Transformation().width(width).height(height).crop(type).angle("exif")).generate(this.public_id + "." + this.format).trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    public JSONObject toJSON(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("libelle", this.libelle);
            jsonObject.put("format", this.format);
            jsonObject.put("full", this.url);
            jsonObject.put("thumb", this.resize(200, 150, CloudinaryUtil.TYPE_FIT));
            jsonObject.put("category", this.category.toJSON(false));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String toString(){
        return this.libelle;
    }
}
