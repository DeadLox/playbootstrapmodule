package models;

import controllers.CRUD;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cleborgne on 22/05/2014.
 */
@Entity
public class Etat extends Data {
    @Required
    public String libelle;
    public String css;
    @CRUD.Hidden
    @OneToMany(mappedBy = "etat")
    public List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

    public String toString(){
        return this.libelle;
    }

    /**
     * Retourne l'état Activé
     * @return Etat Activé
     */
    public static Etat getEnable(){
        return Etat.find("byLibelle", "Activé").first();
    }

    /**
     * Retourne l'état En attente
     * @return Etat En attente
     */
    public static Etat getwaiting(){
        return Etat.find("byLibelle", "En attente").first();
    }

    /**
     * Retourne l'état Désactivé
     * @return Etat Désactivé
     */
    public static Etat getDisable(){
        return Etat.find("byLibelle", "Désactivé").first();
    }

    /**
     * Retourne l'état Désactivé
     * @return Etat Désactivé
     */
    public static Etat getDelete(){
        return Etat.find("byLibelle", "Supprimé").first();
    }

    /**
     * Retourne <code>TRUE</code> si l'état est "Activé"
     * @return <code>TRUE</code> si Activé
     */
    public boolean isEnable(){
        Etat etatEnable = getEnable();
        if (this.equals(etatEnable)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne <code>TRUE</code> si l'état est "En attente"
     * @return <code>TRUE</code> si En attente
     */
    public boolean isWaiting(){
        Etat etatWainting = getwaiting();
        if (this.equals(etatWainting)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne <code>TRUE</code> si l'état est "Désactivé"
     * @return <code>TRUE</code> si Désactivé
     */
    public boolean isDisable(){
        Etat etatDisable = getDisable();
        if (this.equals(etatDisable)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne <code>TRUE</code> si l'état est "Supprimé"
     * @return <code>TRUE</code> si Supprimé
     */
    public boolean isDelete(){
        Etat etatDelete = getDelete();
        if (this.equals(etatDelete)) {
            return true;
        }
        return false;
    }
}
