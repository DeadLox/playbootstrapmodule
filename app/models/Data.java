package models;

import controllers.CRUD;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreUpdate;
import java.util.Date;

/**
 * Created by cleborgne on 27/02/14.
 */
@MappedSuperclass
public abstract class Data extends Model {
    @CRUD.Hidden
    public Date dateCreate;
    @CRUD.Hidden
    public Date dateUpdate;

    public Data(){
        this.dateCreate = new Date();
        this.dateUpdate = this.dateCreate;
    }

    public boolean checkIntegrity(){
        return true;
    }
}
