package models;

import controllers.CRUD;
import org.json.JSONException;
import org.json.JSONObject;
import play.data.validation.Email;
import play.data.validation.Password;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;
import util.CloudinaryUtil;

import javax.persistence.*;
import java.util.*;

/**
 * Created by cleborgne on 28/04/14.
 */
@Entity
public class Utilisateur extends Data {
    @Required
    @Unique
    public String pseudo;
    @Required
    @Email
    @Unique
    public String email;
    @Password
    public String password;
    @Required
    @ManyToMany
    @OrderBy("level DESC")
    public Set<Role> roles = new TreeSet<Role>();
    @Required
    @ManyToOne
    public Etat etat;

    @CRUD.Hidden
    @OneToOne
    public CloudinaryImage avatar;

    @CRUD.Hidden
    public String code;
    @CRUD.Hidden
    public Date codeDate;
    @CRUD.Hidden
    public Date dateCreation;
    @CRUD.Hidden
    public Date dateActivation;
    @CRUD.Hidden
    public Date dateLastLogin;
    @CRUD.Hidden
    public Date dateLastLogin2;

    /** Forum */
    @CRUD.Hidden
    @OneToMany(mappedBy = "auteur")
    public Set<Forum> forums = new TreeSet<Forum>();
    @CRUD.Hidden
    @OneToMany(mappedBy = "auteur")
    public Set<Topic> topics = new TreeSet<Topic>();
    @CRUD.Hidden
    @OneToMany(mappedBy = "auteur")
    public Set<Post> posts = new TreeSet<Post>();
    @CRUD.Hidden
    @OneToMany(mappedBy = "utilisateur")
    public List<ForumView> forumViews = new ArrayList<ForumView>();

    public Utilisateur(){
    }

    /**
     * Vérifie si l'utilsateur à au moins le rôle minimum passé en paramètre
     * @param roleName le rôle minimum a testé
     * @return <code>TRUE</code> si l'utilisateur possède le rôle minimum
     */
    public boolean hasMinRole(String roleName){
        Role minRole = Role.find("byNom", roleName).first();
        if (minRole != null && this.getHighterRole().level >= minRole.level) {
            return true;
        }
        return false;
    }

    /**
     * Retourne le rôle le plus haut
     * @return
     */
    public Role getHighterRole(){
        return this.roles.iterator().next();
    }

    /**
     * Retourne le rôle le plus bas
     * @return
     */
    public Role getLowerRole(){
        if (this.roles.size() > 0) {
            return ((TreeSet<Role>) this.roles).last();
        }
        return this.roles.iterator().next();
    }

    /**
     * Retourne <code>TRUE</code> si l'utilisateur possède le rôle Contributeur
     * @return
     */
    public boolean isContributeur(){
        Role roleContributeur = Role.getRoleContributeur();
        if (this.roles.contains(roleContributeur)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne <code>TRUE</code> si l'utilisateur possède le rôle Admin
     * @return
     */
    public boolean isAdmin(){
        Role roleAdmin = Role.getRoleAdmin();
        if (this.roles.contains(roleAdmin)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne <code>TRUE</code> si l'utilisateur possède le rôle Super Administrateur
     * @return
     */
    public boolean isSuperAdmin(){
        Role roleSuperAdmin = Role.getRoleSuperAdmin();
        if (this.roles.contains(roleSuperAdmin)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne l'object au format JSON pour l'indexation
     * @return JSONObject de l'utilisateur
     */
    public JSONObject toJson(){
        JSONObject userJson = new JSONObject();
        try {
            userJson.put("id", this.id);
            userJson.put("email", this.email);
            userJson.put("etat", this.etat.libelle);
            //userJson.put("roles", this.roles);
            userJson.put("dateCreate", this.dateCreate);
            userJson.put("dateUpdate", this.dateUpdate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userJson;
    }

    /**
     * Met à jour la date de dernière connexion
     */
    public void updateLastLogin(){
        if (this.dateLastLogin != null) {
            this.dateLastLogin2 = this.dateLastLogin;
        }
        this.dateLastLogin = new Date();
        this.save();
    }

    /**
     * Supprime l'avatar de l'utilisateur
     */
    public void removeAvatar(){
        if (avatar != null) {
            CloudinaryImage avatarUser = avatar;
            avatar = null;
            this.save();
            CloudinaryUtil.delete(avatarUser);
        }
    }

    public String toString(){
        return this.pseudo;
    }
}
