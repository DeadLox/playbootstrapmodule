package models;

import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by cleborgne on 06/06/2014.
 */
@Entity
public class Configuration extends Model implements Comparable<Configuration> {
    @Required
    @Unique
    public String key;
    @Required
    public String type;
    @ManyToOne
    public Portee portee;
    public String description;
    public String value;

    public String toString(){
        return this.key;
    }

    /**
     * Retourne le rôle associé
     * @return
     */
    public Role getRole(){
        if (this.value != null && !this.value.equals("")) {
            return Role.findById(Long.parseLong(this.value));
        }
        return null;
    }

    @Override
    public int compareTo(Configuration o) {
        return this.portee.compareTo(o.portee);
    }
}
