package models;

import controllers.CRUD;
import controllers.Security;
import play.data.validation.Required;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 * Created by DeadLox on 24/03/15.
 */
@MappedSuperclass
public class DataForum extends Data {
    @CRUD.Hidden
    @Required
    @ManyToOne
    public Utilisateur auteur;

    /** Droits d'accès */
    @ManyToOne
    public Role viewableBy;
    @ManyToOne
    public Role contribBy;
    @ManyToOne
    public Role editableBy;

    /** Informations */
    public int ordre;
    public boolean locked;

    /**
     * Vérifie si l'utilisateur est l'auteur de la publication
     * @param user L'utilisateur à tester
     * @return <code>TRUE</code> si l'utilisateur est l'auteur
     */
    public boolean isAuthor(Utilisateur user){
        return this.auteur.equals(user);
    }

    /**
     * Vérifie si l'utilisateur à le rôle suffisant pour voir le forum
     * @param user L'utilisateur
     * @return <code>TRUE</code> si l'utilisateur a un rôle suffisant pour voir le Forum
     */
    public boolean canView(Utilisateur user){
        if (user == null) {
            user = Security.getLoggedMember();
        }
        if (this.viewableBy == null) {
            return true;
        }
        return (this.viewableBy.level <= user.getHighterRole().level || this.auteur.equals(user));
    }

    /**
     * Vérifie si l'utilisateur à le rôle suffisant pour contribuer le forum
     * @param user L'utilisateur
     * @return <code>TRUE</code> si l'utilisateur a un rôle suffisant pour contribuer le Forum
     */
    public boolean canContrib(Utilisateur user){
        if (user == null) {
            user = Security.getLoggedMember();
        }
        if (this.contribBy == null) {
            return true;
        }
        return this.contribBy.level <= user.getHighterRole().level;
    }

    /**
     * Vérifie si l'utilisateur à le rôle suffisant pour éditer le forum
     * @param user L'utilisateur
     * @return <code>TRUE</code> si l'utilisateur a un rôle suffisant pour éditer le Forum
     */
    public boolean canEdit(Utilisateur user){
        if (user == null) {
            user = Security.getLoggedMember();
        }
        if (this.editableBy == null) {
            return true;
        }
        return (this.editableBy.level <= user.getHighterRole().level || this.auteur.equals(user));
    }
}
