package models;

import controllers.CRUD;
import play.data.validation.Max;
import play.data.validation.Min;
import play.data.validation.Required;
import play.data.validation.Unique;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cleborgne on 28/04/14.
 */
@Entity
public class Role extends Data implements Comparable {
    @Unique
    @Required
    public String nom;
    public String css;
    @Min(-1)
    @Required
    public int level;
    @CRUD.Hidden
    @ManyToMany(mappedBy = "roles")
    public List<Utilisateur> utilisateur = new ArrayList<Utilisateur>();

    /**
     * Retourne le rôle Supprimé
     * @return Le rôle
     */
    public static Role getRoleSupprime(){
        return Role.find("byNom", "Supprimé").first();
    }

    /**
     * Retourne le rôle Désactivé
     * @return Le rôle
     */
    public static Role getRoleDesactive(){
        return Role.find("byNom", "Désactivé").first();
    }

    /**
     * Retourne le rôle Membre
     * @return Le rôle
     */
    public static Role getRoleMembre(){
        return Role.find("byNom", "Membre").first();
    }

    /**
     * Retourne le rôle Contributeur
     * @return Le rôle
     */
    public static Role getRoleContributeur(){
        return Role.find("byNom", "Contributeur").first();
    }

    /**
     * Retourne le rôle Adminsitrateur
     * @return Le rôle
     */
    public static Role getRoleAdmin(){
        return Role.find("byNom", "Administrateur").first();
    }

    /**
     * Retourne le rôle Super Adminsitrateur
     * @return Le rôle
     */
    public static Role getRoleSuperAdmin(){
        return Role.find("byNom", "Super Administrateur").first();
    }

    /**
     * Méthode de comparaison des Objets
     * @param r Le Rôle à comparer
     * @return Le rôle
     */
    public int compareTo(Object r) {
        if (r instanceof Role) {
            Role role = (Role) r;
            if (this.level > role.level) {
                return 1;
            } else {
                return -1;
            }
        }
        return 0;
    }


    public String toString(){
        return this.nom;
    }
}
