package models;

import controllers.Secure;
import org.json.JSONException;
import org.json.JSONObject;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.URL;
import util.CloudinaryUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Created by cleborgne on 25/02/14.
 */
@Entity
public class Document extends Data {
    @Required
    public String title;
    public String description;
    @Required
    @URL
    @MaxSize(2048)
    @Column(length=2048)
    public String url;
    @OneToOne
    public Role roleVisibilite;
    @ManyToOne
    public Category category;

    /**
     * Vérifie les droits sur le fichier
     * @return Le rôle le plus élevé de l'utilisateur
     */
    public boolean canRead(){
        if (this.roleVisibilite != null) {
            Utilisateur loggedMember = Secure.Security.getLoggedMember();
            if (this.roleVisibilite.level <= loggedMember.getHighterRole().level) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    public JSONObject toJSON(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", this.title);
            jsonObject.put("description", this.description);
            jsonObject.put("url", this.url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String toString(){
        return this.title;
    }
}
