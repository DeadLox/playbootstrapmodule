package models;

import play.Logger;
import play.Play;
import play.data.validation.Required;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Http;
import util.BootstrapUtil;
import util.ConfigurationUtil;

import javax.persistence.Entity;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by DeadLox on 11/01/15.
 */
@Entity
public class Online extends Data {
    @Required
    public String ip;
    @Required
    public Date lastActivity;
    public String username;
    public String language;
    public String userAgent;

    public Online(Utilisateur user){
        this.ip = BootstrapUtil.getRealIpFromHeroku();
        this.lastActivity = new Date();
        if (user != null) {
            username = user.toString();
        } else {
            username = Messages.get("logs.online.user.guest");
        }
        Http.Request request = Http.Request.current();
        if (request != null && request.headers != null) {
            if (request.headers.containsKey("accept-language")) {
                String language = request.headers.get("accept-language").value();
                language = language.split(",")[0];
                language = language.split("-")[0];
                this.language = language;
            }
            if (request.headers.containsKey("user-agent")) {
                this.userAgent = request.headers.get("user-agent").value();
            }
        }
    }

    /**
     * Supprime les anciens logs
     */
    public static void cleanOlder(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, -ConfigurationUtil.getIntValue("logs.online.time.max"));
        Date date = calendar.getTime();
        Online.delete("lastActivity < ?", date);
    }
}
