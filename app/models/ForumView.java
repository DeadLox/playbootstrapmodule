package models;

import controllers.Security;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeadLox on 27/03/15.
 */
@Entity
public class ForumView extends Data {
    @ManyToOne
    public Utilisateur utilisateur;
    @ManyToOne
    public Forum forum;
    @ManyToOne
    public Topic topic;
    @ManyToOne
    public Post post;
    public boolean repondu = false;

    /**
     * Vérifie si l'utilisateur a posté dans un des Topics vues
     * @param forumViews La liste des vues de l'utilisateur
     * @return <code>TRUE</code> si l'utilisateur a posté dans l'un des Topics vue
     */
    public static boolean hasPosted(List<ForumView> forumViews){
        for (ForumView forumView : forumViews) {
            if (forumView.repondu) {
                return true;
            }
        }
        return false;
    }

    /**
     * Vérfie si un des topics possède des nouveaux messages
     * @param forumViews La liste des vues de l'utilisateur
     * @return <code>TRUE</code> si il y a un Topic avec des nouveaux messages
     */
    public static boolean hasNewPost(List<ForumView> forumViews){
        for (ForumView forumView : forumViews) {
            if (forumView.hasNewPost()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Si la date de création du dernier post est antérieure à la date de création du post de l'object ForumView alors il n'y a pas de nouveau message
     * @return <code>TRUE</code> si il y a un ou plusierus nouveaux messages dans le Topic
     */
    public boolean hasNewPost(){
        if (topic != null && this.post.dateCreate.before(this.topic.getLastPost().dateCreate)) {
            return true;
        }
        return false;
    }

    /**
     * Si la date de création du dernier post est antérieure à la date de création du post de l'object ForumView alors il n'y a pas de nouveau message
     * @param topic Le Topic
     * @return <code>TRUE</code> si il y a un ou plusierus nouveaux messages dans le Topic
     */
    public boolean hasNewPost(Topic topic){
        if (topic != null && this.post.dateCreate.before(topic.getLastPost().dateCreate)) {
            return true;
        }
        return false;
    }


    /**
     * Retourne une vue par rapport à un Topic
     * Permet de savoir si l'utilisateur à déjà regardé le Topic
     * De savoir si il à déjà posté un message dans le Topic
     * @param forum Le Forum
     * @return un ForumView
     */
    public static List<ForumView> getFromForum(Forum forum){
        // Vue de l'utilisateur
        Utilisateur loggedMember = Security.getLoggedMember();
        if (loggedMember != null) {
            List<ForumView> forumView = ForumView.find("utilisateur = ? AND forum = ?", loggedMember, forum).fetch();
            if (forumView != null) {
                return forumView;
            }
        }
        return null;
    }


    /**
     * Retourne une vue par rapport à un Topic
     * Permet de savoir si l'utilisateur à déjà regardé le Topic
     * De savoir si il à déjà posté un message dans le Topic
     * @param topic Le topic
     * @return un ForumView
     */
    public static ForumView getFromTopic(Topic topic){
        // Vue de l'utilisateur
        Utilisateur loggedMember = Security.getLoggedMember();
        if (loggedMember != null) {
            ForumView forumView = ForumView.find("utilisateur = ? AND topic = ?", loggedMember, topic).first();
            if (forumView != null) {
                return forumView;
            }
        }
        return null;
    }

    /**
     * Met à jour les derniers Topics et Post vus par l'utilisateur connecté en précisant
     * @param topic Le Topic visualisé
     */
    public static void updateUserView(Topic topic){
        // Vue de l'utilisateur
        Utilisateur loggedMember = Security.getLoggedMember();
        if (loggedMember != null) {
            ForumView forumView = ForumView.find("utilisateur = ? AND topic = ?", loggedMember, topic).first();
            if (forumView == null) {
                // On crée une nouvelle vue
                forumView = new ForumView();
                forumView.utilisateur = loggedMember;
                forumView.topic = topic;
                forumView.forum = topic.forum;
                forumView.post = topic.getLastPost();
                forumView.repondu = false;
                forumView.save();
            } else {
                // On met juste à jour le dernier Post vu
                forumView.post = topic.getLastPost();
                forumView.save();
            }
        }
    }

    /**
     * Met à jour les derniers Topics et Post vus par l'utilisateur connecté en précisant qu'il a répondu à un Topic
     * @param topic Le Topic visualisé
     */
    public static void updateUserPostInTopic(Topic topic){
        // Vue de l'utilisateur
        Utilisateur loggedMember = Security.getLoggedMember();
        if (loggedMember != null) {
            ForumView forumView = ForumView.find("utilisateur = ? AND topic = ?", loggedMember, topic).first();
            if (forumView != null) {
                // On met juste à jour le dernier Post vu
                forumView.post = topic.getLastPost();
                forumView.repondu = true;
                forumView.save();
            }
        }
    }

    public String toString(){
        return this.utilisateur+" - "+this.forum+" "+this.post+" - "+this.post.id;
    }
}
