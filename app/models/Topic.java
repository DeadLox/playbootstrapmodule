package models;

import controllers.CRUD;
import controllers.Security;
import play.data.validation.Required;

import javax.persistence.*;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by cleborgne on 22/01/2015.
 */
@Entity
public class Topic extends DataForum {
    @Required
    public String titre;
    public String description;
    @Required
    @OneToOne
    public Forum forum;
    @OneToMany(mappedBy = "topic")
    @CRUD.Hidden
    public Set<Post> posts = new TreeSet<Post>();

    /** Statistiques */
    @CRUD.Hidden
    public int views;

    /**
     * Retourne l'objet ForumView si l'utilisateur a déjà visité le topic
     * @return L'objet ForumView
     */
    public static ForumView getTopicView(Topic topic){
        Utilisateur loggedMember = Security.getLoggedMember();
        ForumView forumView = ForumView.find("utilisateur = ? AND topic = ?", loggedMember, topic).first();
        if (forumView != null) {
            return forumView;
        }
        return null;
    }

    /**
     * Vérifie si le Topic ou son Forum est vérrouiller
     * @return <code>TRUE</code> si le Topic ou son Forum est vérrouiller
     */
    public boolean isLocked(){
        if (this.forum.locked || this.locked) {
            return true;
        }
        return false;
    }

    /**
     * Retourne le premier post du Topic
     * @return Le premier post du Topic
     */
    public Post getFirstPost(){
        if (this.posts.size() > 0) {
            return (Post) this.posts.toArray()[0];
        }
        return null;
    }

    /**
     * Retourne le dernier Post du Topic
     * @return Le dernier Post
     */
    public Post getLastPost(){
        if (this.posts.size() > 0) {
            return (Post) this.posts.toArray()[this.posts.size()-1];
        }
        return null;
    }

    public String toString(){
        return this.titre;
    }
}
