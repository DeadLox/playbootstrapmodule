package models;

import controllers.CRUD;
import org.hibernate.annotations.Type;
import play.data.validation.Required;

import javax.persistence.*;

/**
 * Created by cleborgne on 22/01/2015.
 */
@Entity
public class Post extends Data implements Comparable<Post> {
    @Required
    @CRUD.Wysiwyg(toolbar = "bbcode")
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    public String contenu;
    @Required
    @ManyToOne
    public Utilisateur auteur;
    @Required
    @ManyToOne
    public Topic topic;

    public String toString(){
        return this.topic.titre;
    }

    @Override
    public int compareTo(Post o) {
        return this.dateCreate.compareTo(o.dateCreate);
    }
}
