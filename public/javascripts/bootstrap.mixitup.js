/* Options */
var mixitupAction;
var mixitup;

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

    mixitup = $('#content-list');
    $('#content-list').on('click', '.category', function(){
        var id = $(this).data('id');
        mixitupLoad(id);
    });
});

function mixitupLoad(id){
    if(mixitup.mixItUp('isLoaded')){
        mixitup.mixItUp('destroy', true);
    }
    $.ajax({
        type: "POST",
        url: mixitupAction({id:id}),
        success: function(data){
            refreshList(data);
        }
    })
}

function refreshList(data){
    mixitup.find('.mix').remove();
    mixitup.find('.alert').remove();
    if (data.childrenSet.length == 0 && data.images.length == 0 && data.documents.length == 0) {
        mixitup.html('<div class="alert alert-info">'+mixitupEmptyResult+'</div>')
    }
    if (data.parent) {
        mixitup.append(createParentCategoryBox(data.parent));
    }
    jQuery.each(data.childrenSet, function(index, category) {
        mixitup.append(createCategoryBox(category));
    });
    jQuery.each(data.images, function(index, image) {
        mixitup.append(createImageBox(image));
    });
    jQuery.each(data.files, function(index, file) {
        mixitup.append(createFileBox(file));
    });
    jQuery.each(data.documents, function(index, document) {
        mixitup.append(createDocumentBox(document));
    });
    mixitup.show().mixItUp({
        load: {
            sort: 'type:asc title:asc'
        },
        callbacks: {
            onMixFail: function(state){
                console.log('No elements found matching '+state.activeFilter);
            }
        }
    });

    if (mixitup.mixItUp('isMixing')) {
        $('.mixitup-load').show();
    }
    if (mixitup.mixItUp('isLoaded')) {
        $('.mixitup-load').hide();
    }

    $('.mix').magnificPopup({
        delegate: 'a.mix-zoom',
        type:'image',
        image: {
            verticalFit: true
        },
        image: {
            titleSrc: function(item) {
                return item.el.data('title') + '<small>'+item.el.data('desc')+'</small>';
            }
        }
    });
}

// Création d'une Box pour la catégorie parente
function createParentCategoryBox(category){
        var divBoxWrapper = $('<div/>');
        divBoxWrapper.addClass('col-sm-3 mix category1');
        divBoxWrapper.attr('data-order', "0");
        divBoxWrapper.attr('data-title', category.title);
        divBoxWrapper.attr('data-id', category.id);
        divBoxWrapper.attr('data-type', "category1");
        divBoxWrapper.addClass('category');

        var divBox = $('<div/>');
        divBox.addClass('ec-box');

        // Title
        var divBoxHeader = $('<div/>');
        divBoxHeader.addClass('ec-box-header');
        divBox.append(divBoxHeader);

        var divBoxHeaderTitle = $('<a/>');
        divBoxHeaderTitle.text(category.title);
        divBoxHeader.append(divBoxHeaderTitle);

        // Content
        var divBoxContent = $('<i/>');
        divBoxContent.attr('class', 'ion-reply icon-folder');
        divBox.append(divBoxContent);

        divBoxWrapper.append(divBox);

        return divBoxWrapper;
}

// Création d'une Box pour une catégorie
function createCategoryBox(category){
        var divBoxWrapper = $('<div/>');
        divBoxWrapper.addClass('col-sm-3 mix category2');
        divBoxWrapper.attr('data-order', "1");
        divBoxWrapper.attr('data-title', category.title);
        divBoxWrapper.attr('data-id', category.id);
        divBoxWrapper.attr('data-type', "category2");
        divBoxWrapper.addClass('category');

        var divBox = $('<div/>');
        divBox.addClass('ec-box');

        // Title
        var divBoxHeader = $('<div/>');
        divBoxHeader.addClass('ec-box-header');
        divBox.append(divBoxHeader);

        var divBoxHeaderTitle = $('<a/>');
        divBoxHeaderTitle.text(category.title);
        divBoxHeaderTitle.attr('title', category.title);
        divBoxHeaderTitle.attr('data-toggle', 'tooltip');
        divBoxHeaderTitle.attr('data-placement', 'top');
        divBoxHeader.append(divBoxHeaderTitle);

        // Content
        var divBoxContent = $('<i/>');
        divBoxContent.attr('class', 'ion-folder icon-folder');
        divBox.append(divBoxContent);

        divBoxWrapper.append(divBox);

        return divBoxWrapper;
}

// Création d'une Box pour une image
function createImageBox(image){
        var divBoxWrapper = $('<div/>');
        divBoxWrapper.addClass('col-sm-3 mix image');
        divBoxWrapper.attr('data-order', "1");
        divBoxWrapper.attr('data-title', image.libelle);
        divBoxWrapper.attr('data-type', "image");

        var divBox = $('<div/>');
        divBox.addClass('ec-box');

        // Title
        var divBoxHeader = $('<div/>');
        divBoxHeader.addClass('ec-box-header');
        divBox.append(divBoxHeader);

        var divBoxHeaderTitle = $('<a/>');
        divBoxHeaderTitle.text(image.libelle);
        divBoxHeaderTitle.attr('title', image.libelle);
        divBoxHeaderTitle.attr('data-toggle', 'tooltip');
        divBoxHeaderTitle.attr('data-placement', 'top');
        divBoxHeader.append(divBoxHeaderTitle);

        // Content
        var divBoxContent = $('<a/>');
        divBoxContent.attr('href', image.full);
        divBoxContent.attr('class', 'mix-zoom ec-box-content');
        divBoxContent.attr('data-title', image.libelle);
        divBoxContent.attr('data-desc', image.category.description);
        divBox.append(divBoxContent);

        var divBoxImage = $('<img/>');
        divBoxImage.attr('src', image.thumb);
        divBoxContent.append(divBoxImage);

        // Footer
        var divBoxFooter = $('<div/>');
        divBoxFooter.addClass('ec-box-footer');
        divBox.append(divBoxFooter);

        var divBoxFooterDownload = $('<a/>');
        divBoxFooterDownload.attr('href', image.full);
        divBoxFooterDownload.attr('target', '_blank');
        divBoxFooterDownload.addClass('btn btn-primary btn-sm');
        divBoxFooterDownload.text(' '+downloadLibelle);
        divBoxFooter.append(divBoxFooterDownload);

        var divBoxFooterDownloadIcon = $('<i/>');
        divBoxFooterDownloadIcon.attr('class', 'glyphicon glyphicon-save');
        divBoxFooterDownload.prepend(divBoxFooterDownloadIcon);

        divBoxWrapper.append(divBox);

        return divBoxWrapper;
}

// Création d'une Box pour un Document
function createDocumentBox(document){
        var divBoxWrapper = $('<div/>');
        divBoxWrapper.addClass('col-sm-3 mix document');
        divBoxWrapper.attr('data-order', "1");
        divBoxWrapper.attr('data-title', document.title);
        divBoxWrapper.attr('data-type', "document");

        var divBox = $('<div/>');
        divBox.addClass('ec-box');

        // Title
        var divBoxHeader = $('<div/>');
        divBoxHeader.addClass('ec-box-header');
        divBox.append(divBoxHeader);

        var divBoxHeaderTitle = $('<a/>');
        divBoxHeaderTitle.text(document.title);
        divBoxHeaderTitle.attr('title', document.title);
        divBoxHeaderTitle.attr('data-toggle', 'tooltip');
        divBoxHeaderTitle.attr('data-placement', 'top');
        divBoxHeader.append(divBoxHeaderTitle);

        // Content
        var divBoxContent = $('<i/>');
        divBoxContent.attr('class', 'ion-document-text icon-document');
        divBox.append(divBoxContent);

        // Footer
        var divBoxFooter = $('<div/>');
        divBoxFooter.addClass('ec-box-footer');
        divBox.append(divBoxFooter);

        var divBoxFooterDownload = $('<a/>');
        divBoxFooterDownload.attr('href', document.url);
        divBoxFooterDownload.attr('target', '_blank');
        divBoxFooterDownload.addClass('btn btn-primary btn-sm');
        divBoxFooterDownload.text(' '+downloadLibelle);
        divBoxFooter.append(divBoxFooterDownload);

        var divBoxFooterDownloadIcon = $('<i/>');
        divBoxFooterDownloadIcon.attr('class', 'glyphicon glyphicon-save');
        divBoxFooterDownload.prepend(divBoxFooterDownloadIcon);

        divBoxWrapper.append(divBox);

        return divBoxWrapper;
}

// Création d'une Box pour un File
function createFileBox(document){
        var divBoxWrapper = $('<div/>');
        divBoxWrapper.addClass('col-sm-3 mix document');
        divBoxWrapper.attr('data-order', "1");
        divBoxWrapper.attr('data-title', document.libelle);
        divBoxWrapper.attr('data-type', "document");

        var divBox = $('<div/>');
        divBox.addClass('ec-box');

        // Title
        var divBoxHeader = $('<div/>');
        divBoxHeader.addClass('ec-box-header');
        divBox.append(divBoxHeader);

        var divBoxHeaderTitle = $('<a/>');
        divBoxHeaderTitle.text(document.libelle);
        divBoxHeaderTitle.attr('title', document.libelle);
        divBoxHeaderTitle.attr('data-toggle', 'tooltip');
        divBoxHeaderTitle.attr('data-placement', 'top');
        divBoxHeader.append(divBoxHeaderTitle);

        // Content
        var divBoxContent = $('<div/>');
        divBoxContent.addClass('contentIframe');
        divBox.append(divBoxContent);

        // Chope et affiche le html
        $.get(document.url, function(result){
            var iframeBoxContent = $('<iframe/>');
            divBoxContent.append(iframeBoxContent);
            var doc = iframeBoxContent[0].document;
            if(iframeBoxContent[0].contentDocument)
                doc = iframeBoxContent[0].contentDocument; // For NS6
            else if(iframeBoxContent[0].contentWindow)
                doc = iframeBoxContent[0].contentWindow.document; // For IE5.5 and IE6
            // Put the content in the iframe
            doc.open();
            doc.writeln(result);
            doc.close();
        })

        // Footer
        var divBoxFooter = $('<div/>');
        divBoxFooter.addClass('ec-box-footer');
        divBox.append(divBoxFooter);

        var divBoxFooterShow = $('<a/>');
        divBoxFooterShow.addClass('btn btn-primary btn-sm btn-zoom');
        divBoxFooterShow.text(' Voir');
        divBoxFooter.append(divBoxFooterShow);
        divBoxFooterShow.on('click', function(){
            showPreview(document);
        });

        var divBoxFooterShowIcon = $('<i/>');
        divBoxFooterShowIcon.attr('class', 'glyphicon glyphicon-zoom-in');
        divBoxFooterShow.prepend(divBoxFooterShowIcon);

        var divBoxFooterDownload = $('<a/>');
        divBoxFooterDownload.attr('href', document.url);
        divBoxFooterDownload.attr('target', '_blank');
        divBoxFooterDownload.addClass('btn btn-primary btn-sm');
        divBoxFooter.append(divBoxFooterDownload);

        var divBoxFooterDownloadIcon = $('<i/>');
        divBoxFooterDownloadIcon.attr('class', 'glyphicon glyphicon-save');
        divBoxFooterDownload.prepend(divBoxFooterDownloadIcon);

        divBoxWrapper.append(divBox);

        return divBoxWrapper;
}

function showPreview(document){
    var preview = $('<div/>');
    preview.attr('class', 'preview');

    var divBoxFooterShowIcon = $('<i/>');
    divBoxFooterShowIcon.attr('class', 'glyphicon glyphicon-remove preview-close');
    divBoxFooterShowIcon.on('click', function(){
        $('.preview').remove();
    })
    preview.prepend(divBoxFooterShowIcon);

    $.get(document.url, function(result){
        var iframeBoxContent = $('<iframe/>');
        preview.append(iframeBoxContent);
        var doc = iframeBoxContent[0].document;
        if(iframeBoxContent[0].contentDocument)
            doc = iframeBoxContent[0].contentDocument; // For NS6
        else if(iframeBoxContent[0].contentWindow)
            doc = iframeBoxContent[0].contentWindow.document; // For IE5.5 and IE6
        // Put the content in the iframe
        doc.open();
        doc.writeln(result);
        doc.close();
    });

    $('body').append(preview);
}