import org.junit.Test;
import play.mvc.Http;
import play.test.FunctionalTest;

/**
 * Created by cleborgne on 08/08/2014.
 */
public class SecureTest extends FunctionalTest {

    @Test
    public void testLoginPage() {
        Http.Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }
}
